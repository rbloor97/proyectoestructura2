package proyecto;

import Prueba_Arbol.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Administrador
 * @param <E>
 */
public class NodoAB{    
    private String Estado="QUIETO";
    private String Accion="ESTAR QUIETO";
    private NodoAB Izq;
    private NodoAB Der;
   public NodoAB(){
   }


    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getAccion() {
        return Accion;
    }

    public void setAccion(String Accion) {
        this.Accion = Accion;
    }

    public NodoAB getIzq() {
        return Izq;
    }

    public void setIzq(NodoAB Izq) {
        this.Izq = Izq;
    }

    public NodoAB getDer() {
        return Der;
    }

    public void setDer(NodoAB Der) {
        this.Der = Der;
    }
   
   
}
