/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.File;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 *
 * @author adan
 */
public class Ganador {
    
    
    public BorderPane root;
    public String rutaimagen,musica;
    public VBox box;
    public Button salir, ganador,jugar;
    public HBox box1;
    
    
    public Ganador(String rutaimagen, String musica){
        this.rutaimagen = rutaimagen;
        this.musica = musica;
        organizarpanel();
    }
    
    
    
    public void organizarpanel(){
        root = new BorderPane();
        box = new VBox();
        salir = new Button();
        ganador = new Button();
        jugar = new Button();
        box1 = new HBox();
        
        
        root.setStyle("-fx-background-image: url('/imagenes/ganador.jpg'); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Pantalla.ANCHO + " " + Pantalla.ALTO + ";");
        
        
        
        Media sound2 = new Media(new File(musica).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound2);
        mediaPlayer.play();
        
        ganador.setStyle("-fx-background-color: transparent; ");
        ImageView bill = new ImageView(rutaimagen);
	bill.setFitWidth(375);
	bill.setFitHeight(550);
	ganador.setGraphic(bill);
        
        
         ganador.setOnMouseEntered((MouseEvent e) -> {
               ganador.setScaleX(1.1);
               ganador.setScaleY(1.1);
        });
 
        ganador.setOnMouseExited((MouseEvent e) -> {
            ganador.setScaleX(1);
            ganador.setScaleY(1);
        });
        
        
        salir.setStyle("-fx-background-color: transparent; ");
        ImageView image2 = new ImageView("/imagenes/whisboton2.png");
	image2.setFitWidth(175);
	image2.setFitHeight(175);
	salir.setGraphic(image2);
        
        salir.setOnAction(e -> Platform.exit());
        
         salir.setOnMouseEntered((MouseEvent e) -> {
               salir.setScaleX(1.1);
               salir.setScaleY(1.1);
        });
 
        salir.setOnMouseExited((MouseEvent e) -> {
            salir.setScaleX(1);
            salir.setScaleY(1);
        });
        
        jugar.setStyle("-fx-background-color: transparent; ");
        ImageView a = new ImageView("/imagenes/Inicioboton.png");
	a.setFitWidth(175);
	a.setFitHeight(175);
	jugar.setGraphic(a);
        
        jugar.setOnAction(e -> {Proyecto.cambiarVentana(root, new Opciones().getRoot());
                                    mediaPlayer.stop();});
        
         jugar.setOnMouseEntered((MouseEvent e) -> {
               jugar.setScaleX(1.1);
               jugar.setScaleY(1.1);
        });
 
        jugar.setOnMouseExited((MouseEvent e) -> {
            jugar.setScaleX(1);
            jugar.setScaleY(1);
        });
        
        box1.setAlignment(Pos.CENTER);
        box1.setSpacing(25);
        
        box1.getChildren().addAll(jugar,salir);
        box.getChildren().addAll(box1,ganador);
        box.setAlignment(Pos.CENTER);
        box.setSpacing(25);
        root.setCenter(box);
        
        
        
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }
    
    
}
