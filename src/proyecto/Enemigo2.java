/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.awt.Rectangle;
import javafx.scene.image.ImageView;

public class Enemigo2 {
     private String nombre;

    private int cantidadVida, cantidadAtaque,numeroAtaque,numeroAtaqueFinal;

    public int getNumeroAtaqueFinal() {
        return numeroAtaqueFinal;
    }

    public void setNumeroAtaqueFinal(int numeroAtaqueFinal) {
        this.numeroAtaqueFinal = numeroAtaqueFinal;
    }
    private Estado estado;
    private ImageView imagenPrincipal;
    private final String RUTA_IMAGEN_PRINCIPAL = "imagenes/broly/Img1.png";
    
     public Enemigo2() {
        cantidadVida = 150;
        cantidadAtaque = 10;
        numeroAtaque = 6;
        numeroAtaqueFinal = 2;
        estado = Estado.QUIETO;
        imagenPrincipal = new ImageView(RUTA_IMAGEN_PRINCIPAL);
        imagenPrincipal.setFitHeight(325);
        imagenPrincipal.setFitWidth(235);
      
    }

    public int getNumeroAtaque() {
        return numeroAtaque;
    }

    public void setNumeroAtaque(int numeroAtaque) {
        this.numeroAtaque = numeroAtaque;
    }

    public Enemigo2(String nombre, int cantidadVida, int cantidadAtaque) {
        this.nombre = nombre;
        this.cantidadVida = cantidadVida;
        this.cantidadAtaque = cantidadAtaque;
        this.imagenPrincipal = new ImageView(RUTA_IMAGEN_PRINCIPAL);
    
    
    }
    
    public Estado getEstado() {
        return estado;
    }

    //getters and setters
    public void setEstado(Estado estado) {    
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadVida() {
        return cantidadVida;
    }

    public void setCantidadVida(int cantidadVida) {
        this.cantidadVida = cantidadVida;
    }
    
    public int getCantidadAtaque() {
        return cantidadAtaque;
    }

    public void setCantidadAtaque(int cantidadAtaque) {
        this.cantidadAtaque = cantidadAtaque;
    }

 

    public ImageView getImagenPrincipal() {
        return imagenPrincipal;
    }

    public void setImagenPrincipal(ImageView imagenPrincipal) {
        this.imagenPrincipal = imagenPrincipal;
    }
    
    
    //Rectangulos para la colision
    public Rectangle rectanguloEnemigo2(){
        return new Rectangle((int)imagenPrincipal.getLayoutX(),(int)imagenPrincipal.getLayoutY(),
                            (int)imagenPrincipal.getFitWidth(),(int)imagenPrincipal.getFitHeight());
    }
    
    //colision con player
    public boolean collision(Player player){
        return this.rectanguloEnemigo2().intersects(player.rectanguloPlayer());
    }
}