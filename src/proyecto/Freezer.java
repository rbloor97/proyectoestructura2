/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.awt.Rectangle;
import java.util.List;
import java.util.Map;
import javafx.animation.PathTransition;
import javafx.scene.image.ImageView;

/**
 *
 * @author adan
 */
public class Freezer {
     private String nombre;
    private int cantidadVida, cantidadAtaque,numeroAtaque,numeroAtaqueFinal;

    public void setNumeroAtaqueFinal(int numeroAtaqueFinal) {
        this.numeroAtaqueFinal = numeroAtaqueFinal;
    }

    public int getNumeroAtaqueFinal() {
        return numeroAtaqueFinal;
    }
    private Estado estado;
    private ImageView imagenPrincipal;
    private final String RUTA_IMAGEN_PRINCIPAL = "imagenes/sprites/Bill/bill.png";
    
    
     public Freezer() {
        nombre = "";
        cantidadVida = 100;
        cantidadAtaque = 10;
        numeroAtaque = 6;
        numeroAtaqueFinal = 1;
       // estado = Estado.QUIETO;
        imagenPrincipal = new ImageView(RUTA_IMAGEN_PRINCIPAL);
        imagenPrincipal.setFitHeight(245);
        imagenPrincipal.setFitWidth(145);
        imagenPrincipal.setLayoutX(Pantalla.ANCHO/3);
        imagenPrincipal.setLayoutY(Pantalla.ALTO/2);
    
    }
     
     private String Accion;

    public String getAccion() {
        return Accion;
    }

    public void setAccion(String Accion) {
        this.Accion = Accion;
    }

    public int getNumeroAtaque() {
        return numeroAtaque;
    }

    public void setNumeroAtaque(int numeroAtaque) {
        this.numeroAtaque = numeroAtaque;
    }

    public Freezer(String nombre, int cantidadVida, int cantidadAtaque) {
        this.nombre = nombre;
        this.cantidadVida = cantidadVida;
        this.cantidadAtaque = cantidadAtaque;
        this.imagenPrincipal = new ImageView(RUTA_IMAGEN_PRINCIPAL);
    
    
    }
    
    public Estado getEstado() {
        return estado;
    }

    //getters and setters
    public void setEstado(Estado estado) {    
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadVida() {
        return cantidadVida;
    }

    public void setCantidadVida(int cantidadVida) {
        this.cantidadVida = cantidadVida;
    }
    
    public int getCantidadAtaque() {
        return cantidadAtaque;
    }

    public void setCantidadAtaque(int cantidadAtaque) {
        this.cantidadAtaque = cantidadAtaque;
    }

 

    public ImageView getImagenPrincipal() {
        return imagenPrincipal;
    }

    public void setImagenPrincipal(ImageView imagenPrincipal) {
        this.imagenPrincipal = imagenPrincipal;
    }
    
  
    
    @Override
    public String toString(){
        return "\nNombre: "+this.getNombre()+"\nPuntos de Vida: "+this.getCantidadVida()+
                "\nAtaque: "+this.getCantidadAtaque();
    }
    
    //Rectangulos para la colision
    public Rectangle rectanguloPlayerF(){
        return new Rectangle((int)imagenPrincipal.getLayoutX(),(int)imagenPrincipal.getLayoutY(),
                            (int)imagenPrincipal.getFitWidth(),(int)imagenPrincipal.getFitHeight());
    }
    
    //colision con player
    public boolean collision(Player player){
        return this.rectanguloPlayerF().intersects(player.rectanguloPlayer());
    }
}
