/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 *
 * @author Administrador
 */
public class Proyecto extends Application {
    String ruta;
    
    @Override
    public void start(Stage primaryStage) {
        
        Scene scene = new Scene(new Menu().getRoot(), Pantalla.ANCHO, Pantalla.ALTO);
        
        
        //scene.onKeyPressedProperty().bind(new PaneOrganizer(ruta).getRoot().onKeyPressedProperty());
        
        primaryStage.setTitle("Fighter!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    /*
    *@root El pane actual de la ventana.
    *@root2 El pane de la ventana que quieres ir.
    */
     public static void cambiarVentana(Pane root, Pane root2 ){
        Scene scn = root.getScene();
         Stage stage = (Stage) scn.getWindow();
         Scene scn2 = new Scene( root2,Pantalla.ANCHO, Pantalla.ALTO );
         
         stage.setScene(scn2);
    }
     
      public static void ponerMusica(String ruta) {
        Media media = new Media(new File(ruta).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setAutoPlay(true);
        MediaView mediaView = new MediaView(mediaPlayer);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
