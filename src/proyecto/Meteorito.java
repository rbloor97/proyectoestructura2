/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

/**
 *
 * @author adan
 */
import javax.swing.Timer;
import java.awt.event.ActionListener;
import java.util.Random;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.util.Duration;
import static proyecto.PaneOrganizer.player;

public class Meteorito {
 
   
    private static final String ARCHIVO_IMAGEN="imagenes/meteorito.gif";
    private ImageView dinero;

    private StackPane pane;
    private PathTransition transition;
    private Player player;
    private Freezer playerF;
    private Enemigo2 playerF2;
    private Timer timer;

   
   
    Meteorito(Player player,Freezer playerF){
        this.player=player;
        this.playerF=playerF;
        pane = new StackPane();
       
        dinero= new ImageView(new Image(ARCHIVO_IMAGEN));
        dinero.setFitHeight(150);
        dinero.setFitWidth(100);
        
         //text.setFill(Color.WHITE); 
       
        agregarPane();
        animacion(pane);
      
    }
    
    //cnstructor para paneOrganizer2
    Meteorito(Player player,Enemigo2 playerF2){
        this.player=player;
        this.playerF2=playerF2;
        pane = new StackPane();
       
        dinero= new ImageView(new Image(ARCHIVO_IMAGEN));
        dinero.setFitHeight(150);
        dinero.setFitWidth(100);
        
         //text.setFill(Color.WHITE); 
       
        agregarPane();
        animacion2(pane);
      
    }
    

    public void agregarPane(){
        pane.getChildren().addAll(dinero);
    }

    
    
    public ImageView getDinero(){ 
        return dinero;
            
        
    }


    public void setDinero(ImageView dinero) {   
        this.dinero = dinero;
    }
    public PathTransition getTransition() {
        return transition;
    }
    public Timer getTimer() {
        return timer;
    }

    public void animacion(Node n) {
        transition = new PathTransition();
        transition.setNode(n);
 
  
        Random r = new Random();
        
        int num = r.nextInt((int) 700);
        Line linea = new Line();
        linea.setStartX(num+550);
        linea.setStartY(0);
        linea.setEndX(0);
        linea.setEndY(Pantalla.ALTO+100);
        
        transition.setPath(linea);
        
        transition.setDuration(Duration.seconds(6));
  
        transition.setCycleCount(1);
  
        transition.setAutoReverse(false);
        timer = new Timer (700, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE METEORITO");
               checkCollision();
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
      
        transition.play();
    }
    
    //animacion para Paneorganizer2
    public void animacion2(Node n) {
        transition = new PathTransition();
        transition.setNode(n);
 
  
        Random r = new Random();
        
        int num = r.nextInt((int) 700);
        Line linea = new Line();
        linea.setStartX(num+550);
        linea.setStartY(0);
        linea.setEndX(0);
        linea.setEndY(Pantalla.ALTO+100);
        
        transition.setPath(linea);
        
        transition.setDuration(Duration.seconds(6));
  
        transition.setCycleCount(1);
  
        transition.setAutoReverse(false);
        timer = new Timer (700, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE METEORITO");
               checkCollision2();
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
      
        transition.play();
    }
    
    public void animacionfinal(Node n) {
        transition = new PathTransition();
        transition.setNode(n);
 
  
        Random r = new Random();
        
        int num = r.nextInt((int) 1010);
        Line linea = new Line();
        linea.setStartX(num);
        linea.setStartY(0);
        linea.setEndX(num);
        linea.setEndY(Pantalla.ALTO);
        
        transition.setPath(linea);
        
        transition.setDuration(Duration.seconds(2));
  
        transition.setCycleCount(1);
  
        transition.setAutoReverse(false);
      
        transition.play();
    }


    public StackPane getPane() {
        return pane;
    }

    public void setPane(StackPane pane) {
        this.pane = pane;
    }

    Object getBoundsInParent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    //colisiones
    public void checkCollision(){
        if(this.collision(player)){          
            System.out.println("CHOQUE METEORITO CONTRA GOKU DETECTADO");
           // pane.getChildren().remove(dinero);
            player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/herido.png"));
          //  pane.getChildren().removeAll(this.dinero);
            
            
            if(player.getCantidadVida()>0)
                player.setCantidadVida(player.getCantidadVida()-5);   
            timer.stop();
        }
        else if(this.collision(playerF)){
            System.out.println("CHOQUE METEORITO CONTRA BILLS DETECTADO");
            if(playerF.getCantidadVida()>0){
                playerF.setCantidadVida(playerF.getCantidadVida()-5); 
                playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/herido.png"));
            }
                
            timer.stop();
        }
    }
    
    //colision en paneorganizer2
    public void checkCollision2(){
        if(this.collision(player)){          
            System.out.println("CHOQUE METEORITO CONTRA VEGETA DETECTADO");
           // pane.getChildren().remove(dinero);
            player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/herido.png"));
          //  pane.getChildren().removeAll(this.dinero);
            
            
            if(player.getCantidadVida()>0)
                player.setCantidadVida(player.getCantidadVida()-5);   
            timer.stop();
        }
        else if(this.collision(playerF2)){
            System.out.println("CHOQUE METEORITO CONTRA ENEMIGO2 DETECTADO");
            if(playerF2.getCantidadVida()>0)
                playerF2.setCantidadVida(playerF2.getCantidadVida()-0);  
            timer.stop();
        }
    }
    
    //colision con jugador 
    public boolean collision(Player player){
        PathTransition transicion = this.getTransition();
        return player.rectanguloPlayer().intersects(transicion.getNode().translateXProperty().doubleValue(),
                                                transicion.getNode().translateYProperty().doubleValue(),
                                                75,50);
    }
    
    //colision con freezer
    public boolean collision(Freezer playerF){
        PathTransition transicion = this.getTransition();
        return playerF.rectanguloPlayerF().intersects(transicion.getNode().translateXProperty().doubleValue(),
                                                transicion.getNode().translateYProperty().doubleValue(),
                                                75,50);
    }
    
    //colision con enemigo en paneorganizer2
    public boolean collision(Enemigo2 playerF2){
        PathTransition transicion = this.getTransition();
        return playerF2.rectanguloEnemigo2().intersects(transicion.getNode().translateXProperty().doubleValue(),
                                                transicion.getNode().translateYProperty().doubleValue(),
                                                75,50);
    }
    
}


