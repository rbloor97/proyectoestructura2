/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.File;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 *
 * @author adan
 */
public class Menu {
    
private BorderPane root;
    private Button inicio, salir,instrucciones;
    

    private VBox box;

    
    Menu() {
        
        
        organizarpanel();
       
    }
    
    public void organizarpanel(){
        root = new BorderPane();
        box = new VBox();

        
        inicio = new Button();
        instrucciones = new Button();
      
        salir = new Button();
        
        
        Media sound2 = new Media(new File("src/musica/budokai.mp3").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound2);
        mediaPlayer.play();
        
      

        inicio.setStyle("-fx-background-color: transparent; ");
        ImageView image = new ImageView("/imagenes/jugargoku.png");
	image.setFitWidth(260);
	image.setFitHeight(150);
	inicio.setGraphic(image);
        
        inicio.setOnMouseEntered((MouseEvent e) -> {
            inicio.setScaleX(1.1);
            inicio.setScaleY(1.1);
        });
 
        inicio.setOnMouseExited((MouseEvent e) -> {
            inicio.setScaleX(1);
            inicio.setScaleY(1);
        });
        
        inicio.setOnAction(e -> {Proyecto.cambiarVentana(root, new Opciones().getRoot());
        mediaPlayer.stop();
          ArbolBinario abb=new ArbolBinario();

    NodoAB n1= new NodoAB(); 
    n1.setEstado("QUIETO");
    NodoAB n2= new NodoAB();
    n2.setAccion("atacar");
    NodoAB n3= new NodoAB();
    n3.setEstado("DERECHA");
    NodoAB n4= new NodoAB();
    n4.setAccion("va a la izquierda");
    NodoAB n5= new NodoAB();
    n5.setEstado("Izqu");
    NodoAB n6= new NodoAB();
    n6.setAccion("Va a la izquierda");
    NodoAB n7= new NodoAB();
    n7.setEstado("ATACA");
    NodoAB n8= new NodoAB();
    n8.setAccion("se cubre");
    NodoAB n9= new NodoAB();
    n9.setEstado("ATAQUE_FINAL");
    NodoAB n10= new NodoAB();
    n10.setAccion("Cubrirse");
    NodoAB nfinal=new NodoAB();
    nfinal.setAccion("estar quiero");
    //Creacion arbol
    //creacion nodo raiz
    abb.add(null, n1);
    //creacion raiz-izquierda1
     abb.add(n1, n3);
     //creacion raiz-derecha1
     abb.add(n1, n2);
     //creacion izquierda1-izquierda2
     abb.add(n3, n5);
     //creacion izquierda1-derecha2
     abb.add(n3,n4);
     //creacion izquierda2-izquierda3
      abb.add(n5,n7);
      //creacion izquierda2-derecha3
     abb.add(n5, n6);
     //creacion izquierda3-izquierda4
      abb.add(n7,n9);
      //creacion izquierda3-derecha4
     abb.add(n7, n8);
     //creacion izquierda4-izquierda5
     abb.add(n9, nfinal);
     abb.add(n9,n10);
//    //recorrer arbol
//    //------------
    
        //System.out.println(abb.recorrerarbol(PaneOrganizer.player.getEstado().toString()).getAccion()); 
    });
        
        
        instrucciones.setStyle("-fx-background-color: transparent; ");
        ImageView m = new ImageView("imagenes/botoninstrucciones.png");
	m.setFitWidth(260);
	m.setFitHeight(150);
	instrucciones.setGraphic(m);
        
        instrucciones.setOnMouseEntered((MouseEvent e) -> {
            instrucciones.setScaleX(1.1);
            instrucciones.setScaleY(1.1);
        });
 
        instrucciones.setOnMouseExited((MouseEvent e) -> {
            instrucciones.setScaleX(1);
            instrucciones.setScaleY(1);
        });
        
        instrucciones.setOnAction(e -> {Proyecto.cambiarVentana(root, new Instrucciones().getRoot());
        mediaPlayer.stop();});
        
        salir.setStyle("-fx-background-color: transparent; ");
        ImageView image2 = new ImageView("/imagenes/salirbill.png");
	image2.setFitWidth(260);
	image2.setFitHeight(150);
        
	salir.setGraphic(image2);
        
           salir.setOnMouseEntered((MouseEvent e) -> {
               salir.setScaleX(1.1);
               salir.setScaleY(1.1);
        });
 
        salir.setOnMouseExited((MouseEvent e) -> {
            salir.setScaleX(1);
            salir.setScaleY(1);
        });
        
        salir.setOnAction(e ->Platform.exit());

        
        
        box.getChildren().addAll(inicio,instrucciones,salir);
        
        
        
        root.setStyle("-fx-background-image: url('/imagenes/iniciojuego.jpg'); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Pantalla.ANCHO + " " + Pantalla.ALTO + ";");

        box.setAlignment(Pos.CENTER);
        box.setSpacing(10);
        root.setBottom(box);               
    }
  
  
   
    BorderPane getRoot() {
        return root;
    }

    
}

