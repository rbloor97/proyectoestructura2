/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;

/**
 *
 * @author adan
 */
public class Instrucciones {
    
private BorderPane root;
private Button inicio, salir;
private HBox box;
    
    Instrucciones() {
        
        
        organizarpanel();
        
    }
    
    public void organizarpanel(){
        root = new BorderPane();
        box = new HBox();
        
        inicio = new Button();
      
        salir = new Button();
        
        Media sound2 = new Media(new File("src/musica/theme18.mp3").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound2);
        mediaPlayer.play();
        
      

        inicio.setStyle("-fx-background-color: transparent; ");
        ImageView image = new ImageView("imagenes/brolyjugar.png");
	image.setFitWidth(185);
	image.setFitHeight(115);
	inicio.setGraphic(image);
        
        inicio.setOnMouseEntered((MouseEvent e) -> {
            inicio.setScaleX(1.1);
            inicio.setScaleY(1.1);
        });
 
        inicio.setOnMouseExited((MouseEvent e) -> {
            inicio.setScaleX(1);
            inicio.setScaleY(1);
        });
        
        
        
   
        
        
     
        
        
        
        inicio.setOnAction(e -> {Proyecto.cambiarVentana(root, new Opciones().getRoot());
        mediaPlayer.stop();});
        
        
        salir.setStyle("-fx-background-color: transparent; ");
        ImageView image2 = new ImageView("imagenes/salir1.png");
	image2.setFitWidth(185);
	image2.setFitHeight(115);
	salir.setGraphic(image2);
        
           salir.setOnMouseEntered((MouseEvent e) -> {
               salir.setScaleX(1.1);
               salir.setScaleY(1.1);
        });
 
        salir.setOnMouseExited((MouseEvent e) -> {
            salir.setScaleX(1);
            salir.setScaleY(1);
        });
            
        salir.setOnAction(e -> Platform.exit());
        
        
        box.getChildren().addAll(inicio,salir);
        
        
        
        root.setStyle("-fx-background-image: url('/imagenes/InstruccionesBill.png'); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Pantalla.ANCHO + " " + Pantalla.ALTO + ";");

        box.setAlignment(Pos.TOP_LEFT);
        box.setSpacing(50);
        root.setBottom(box);               
        
       
      
        
       
      
        
        
     
        
    
        
         
     
        
        
        
   
    }
  
  
   
    BorderPane getRoot() {
        return root;
    }

    
}


