/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.File;
import java.util.LinkedList;
import java.util.Random;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import static javafx.scene.input.KeyEvent.KEY_RELEASED;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Line;
import javafx.util.Duration;

/**
 *
 * @author Administrador
 */
public final class PaneOrganizer {
    private BorderPane root;
    private Button salir;
    private HBox box;
    static Player player;
    static Freezer playerF;
    Reloj reloj;
    LinkedList<PlayerAtaque> lista= new LinkedList<>();
    private final String nombreimagen;
    private LinkedList<Meteorito> LBasura;
    private Meteorito m2;
    public static int tiempoRestante;
    private PlayerAtaque esposa=null;
    private PlayerAtaque esposa1=null;
    MediaPlayer mediaPlayer;
    Timeline valores;
    private final Thread ejecuta;
    
    public PaneOrganizer(String nombreimagen){
        this.nombreimagen = nombreimagen;
        organizador();
        
        ejecuta = new Thread(new Ejecutaanimacion());
        ejecuta.start();
        manejarTiempo();
    }
    
    
    
    public void organizador(){

       LBasura = new LinkedList<>();
        player = new Player();
        playerF = new Freezer();
        root = new BorderPane();
        salir = new Button();
        reloj = new Reloj(Opciones.tiempoRestante);
        tiempoRestante = reloj.getHora();
        //manejarTiempo();
        Media sound2 = new Media(new File("src/musica/theme5.mp3").toURI().toString());
        mediaPlayer = new MediaPlayer(sound2);
        mediaPlayer.play();
        
        salir.setStyle("-fx-background-color: transparent; ");
        ImageView image2 = new ImageView("/imagenes/salida.png");
	image2.setFitWidth(85);
	image2.setFitHeight(85);
	salir.setGraphic(image2);
        player.getImagenPrincipal().setLayoutX(Pantalla.ANCHO/3);
        player.getImagenPrincipal().setLayoutY(Pantalla.ALTO-245);
        

        playerF.getImagenPrincipal().setLayoutX(Pantalla.ANCHO/1.5);
        playerF.getImagenPrincipal().setLayoutY(Pantalla.ALTO-245);
        
        
        box = new HBox();
        
        box.getChildren().addAll(salir,reloj.getVidavegeta(),reloj.getAtaquevegeta(),reloj.getAtaquefinalvegueta(),reloj.getTiempo(),reloj.getVidafreezer(),reloj.getAtaquefreezer(),reloj.getAtaquefinalfreezer());
        box.setSpacing(15);
        
        root.getChildren().addAll(player.getImagenPrincipal(),playerF.getImagenPrincipal());
        
        root.setOnKeyPressed(e->{moverObjeto(e);
                                    moverPlayer2(e);});
        
        root.setStyle("-fx-background-image: url("+nombreimagen+"); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Pantalla.ANCHO + " " + Pantalla.ALTO + ";");
        root.setTop(box);
        
        //Dando acciones a lso botones
        salir.setOnAction((ActionEvent e) -> {
            ejecuta.stop();
            m2.getTransition().stop();
            m2.getTimer().stop();
            Platform.exit();
       });
        
    }
    
    void manejarTiempo() {
        //crear los timeline y keyframe a utilizar.
        
        valores = new Timeline();
        KeyFrame kfvalores = new KeyFrame(Duration.millis(1), e -> vercampeon());
        valores.getKeyFrames().addAll(kfvalores);
        valores.setCycleCount(Timeline.INDEFINITE);
        valores.play();
      
    }
    
    public void vercampeon(){
        String musica = "src/musica/creacion.mp3";
        String imagenbill = "imagenes/billganador.png";
        String imagengoku = "imagenes/gokudios.png";
        String goku = "src/musica/gokugano.mp3";
               
        if(player.getCantidadVida()<1){            
            reloj.valores.stop();
            reloj.valores1.stop();
            reloj.recuperar.stop();
            reloj.vida.stop();
            mediaPlayer.stop();
            valores.stop();
            
            ejecuta.stop();
            m2.getTimer().stop();
            m2.getTransition().stop(); 
            
            
            
            
           Proyecto.cambiarVentana(root, new Ganador( imagenbill,musica).getRoot());
            
             
            
            
        }
        
        if(playerF.getCantidadVida()<1){
            
            reloj.valores.stop();
            reloj.valores1.stop();
            reloj.recuperar.stop();
            reloj.vida.stop();
            mediaPlayer.stop();
            valores.stop();
            
            ejecuta.stop();
            m2.getTimer().stop();
            m2.getTransition().stop(); 
            
             Proyecto.cambiarVentana(root, new PaneOrganizer2("imagenes/brolyescenario.jpg").getRoot());
           // Proyecto.cambiarVentana(root, new Ganador(imagengoku,goku).getRoot());
            
        }
        
        
        
        if(reloj.hora==0){
            
            reloj.valores.stop();
            reloj.valores1.stop();
            reloj.recuperar.stop();
            reloj.vida.stop();
            mediaPlayer.stop();
            valores.stop();
            
            ejecuta.stop();
            m2.getTransition().stop();  
            m2.getTimer().stop();   
            
            
            tiempoRestante = 0;
            System.out.println("Se acab� el tiempo!!");
            
            
            if(player.getCantidadVida()>playerF.getCantidadVida()){
                System.out.println("a");
                ejecuta.stop();
                m2.getTransition().stop();  
                m2.getTimer().stop(); 
                
                
                Proyecto.cambiarVentana(root, new PaneOrganizer2("imagenes/brolyescenario.jpg").getRoot());
                
               // Proyecto.cambiarVentana(root, new Ganador(imagengoku,goku).getRoot());
            
            }
            
            if(player.getCantidadVida()<playerF.getCantidadVida()){
                System.out.println("n");
                ejecuta.stop();
                m2.getTransition().stop();  
                m2.getTimer().stop();   
                Proyecto.cambiarVentana(root, new Ganador( imagenbill,musica).getRoot());
            
            }
            
            
            if(player.getCantidadVida()==playerF.getCantidadVida()){
                System.out.println("n");
                ejecuta.stop();
                m2.getTransition().stop();  
                m2.getTimer().stop();   
                Proyecto.cambiarVentana(root, new Ganador( imagenbill,musica).getRoot());
            
            }
            
            
            
        }
}
    
    
    public void creaNuevoMeteorito(){
       // Dinero m2 = new Dinero();
        m2 = new Meteorito(player,playerF);
        Random num = new Random();
        int rand = num.nextInt(15);
        
        LBasura.add(m2);     
        
        root.getChildren().add(m2.getPane());
        }
      

    public void remuevePane(MouseEvent a){
            root.getChildren().remove(a.getSource());

        }
    
    public void checkCollision() {

        if (player.collision(playerF)){          
           // System.out.println("CHOQUE GOKU CONTRA BILLS DETECTADO");        
        }      
       }
        //return a.getBoundsInParent().intersects(b.getBoundsInParent());
        
    
    
     public void moverGokuderecha(){
         
         if(player.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO<=Pantalla.ANCHO-100){
                    
            player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/derecha.png"));
            playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/bill.png"));


            player.getImagenPrincipal().setLayoutX(player.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO);
            playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX());
            player.setEstado(Estado.DERECHA);

            inteligenciaMaquina(player.getEstado());
            }
                
         
     }
     
     
     public void Gokusaltar(){
     

        player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/salto.png"));  
        playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/bill.png"));

        player.setEstado(Estado.SALTO);
        animacionSaltar(player.getImagenPrincipal());
        inteligenciaMaquina(player.getEstado());
     }
     
     
     public void moverGokuizquierda(){
         
         if(player.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO>=35){
                   
            player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/retro.png"));  
            playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/bill.png"));

            player.getImagenPrincipal().setLayoutX(player.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO);
            playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX());
             }
             player.setEstado(Estado.IZQUIERDA);
             checkCollision();
             inteligenciaMaquina(player.getEstado());
         
     }
     
     
     
     
     public void ataqueGoku(){
         if(player.getNumeroAtaque()>0){
                esposa = new PlayerAtaque(player,playerF);
                    
                    
                   
                player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/ataque.png")); 
               // playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/bill.png"));
                player.setEstado(Estado.ATAQUE);
                inteligenciaMaquina(player.getEstado());
                 root.getChildren().add(esposa.getAtaqueGoku());
                 lista.add(esposa);
                 for(PlayerAtaque i: lista){
                     i.animacion(player.getImagenPrincipal().getLayoutY()+35).setOnFinished(ev->{root.getChildren().removeAll(i.getAtaqueGoku());});
                 }
                 player.setNumeroAtaque(player.getNumeroAtaque()-1); 
            }  
     }
     
    
     public void Gokucubrirse(){
         
         
        player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/cubrir.png"));
     
        if (player.collision(playerF)){
            player.getImagenPrincipal().setLayoutX(player.getImagenPrincipal().getLayoutX()-10);
            playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX()+10);
        }

        player.setEstado(Estado.AGACHADO);
        checkCollision();
        inteligenciaMaquina(player.getEstado());
         
     }
     
     
     public void ataquefinalGoku(){
         
          if(player.getNumeroAtaquefinal()>0){

                Media sound1 = new Media(new File("src/musica/genkidama1.mp3").toURI().toString());
                MediaPlayer mediaPlayer1 = new MediaPlayer(sound1);
                mediaPlayer1.play();

                player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/genkidama2.gif"));
                esposa1 = new PlayerAtaque(player,playerF);

                //playerF.setCantidadVida(playerF.getCantidadVida()-player.getCantidadAtaquefinal());
                lista.add(esposa1);
                root.getChildren().add(esposa1.getGenkidama());
                for(PlayerAtaque i: lista){
                    
                     i.genkidama(esposa1.getGenkidama()).setOnFinished(ev->{root.getChildren().removeAll(i.getGenkidama());});
                }

                player.setNumeroAtaquefinal(player.getNumeroAtaquefinal()-1); 

            player.setEstado(Estado.ATAQUE_FINAL);
            inteligenciaMaquina(player.getEstado());

        } 
         
     }
     
     
     
     
    public void moverBillizquierda(){
         
         if(playerF.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO+5>=35){       
           
            playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/B4izq.png"));

            playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO-5);
        
            playerF.setEstado(Estado.IZQUIERDA);
       
         }
         
     }
    
      public void moverBillizquierdalento(){
         
         if(playerF.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO+5>=35){       
           
            playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/B4izq.png"));

            playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO-1);
        
            playerF.setEstado(Estado.IZQUIERDA);
       
         }
         
     }
    
     public void moverBillderecha(){
         
         if(playerF.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO-5<=Pantalla.ANCHO-85){                             
          
            playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/B7izq.png"));
            playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO+5);
          
        }
        playerF.setEstado(Estado.DERECHA);
        
         
     }
     
      public void moverBillderechalento(){
         
         if(playerF.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO-5<=Pantalla.ANCHO-85){                             
          
            playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/B7izq.png"));
            playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO);
          
        }
        playerF.setEstado(Estado.DERECHA);
        
         
     }
     
     
    public void Billcubrirse(){
        player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/goku1.png"));
        playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/B6izq.png"));
        if (player.collision(playerF)){
            player.getImagenPrincipal().setLayoutX(player.getImagenPrincipal().getLayoutX()-10);
            playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX()+10);
        }

        playerF.setEstado(Estado.AGACHADO);
               
         
     }
    
    public void Billataque(){
        
         if(playerF.getNumeroAtaque()>0){
             
             
            esposa = new PlayerAtaque(player,playerF);
            playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/billataca.png"));
            playerF.setEstado(Estado.ATAQUE);
            
            lista.add(esposa);
            root.getChildren().add(esposa.getAtaqueBill());
            for(PlayerAtaque i: lista){
               i.animacionAtqEnemigo1(playerF.getImagenPrincipal().getLayoutY()+50).setOnFinished(ev->{root.getChildren().removeAll(i.getAtaqueBill());});
            }
            playerF.setNumeroAtaque(playerF.getNumeroAtaque()-1); 
            
        
                    
         }
         
        
         
     }
    
    
    public void Billataquefinal(){
        
        
        if(playerF.getNumeroAtaqueFinal()>0){

                Media sound1 = new Media(new File("src/musica/hakai.mp3").toURI().toString());
                MediaPlayer mediaPlayer1 = new MediaPlayer(sound1);
                mediaPlayer1.play();

                playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/B9izq.png"));
                esposa1 = new PlayerAtaque(player,playerF);

                //playerF.setCantidadVida(playerF.getCantidadVida()-player.getCantidadAtaquefinal());
                lista.add(esposa1);
                root.getChildren().add(esposa1.getAtaqueFinalBill());
                for(PlayerAtaque i: lista){
                    
                     i.Hakai(esposa1.getAtaqueFinalBill()).setOnFinished(ev->{root.getChildren().removeAll(i.getAtaqueFinalBill());});
                }

                playerF.setNumeroAtaqueFinal(playerF.getNumeroAtaqueFinal()-1); 

           // player.setEstado(Estado.ATAQUE_FINAL);
            //inteligenciaMaquina(player.getEstado());

        } 
         
     }
     
    public void Billsaltar(){
        
        playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/B1izq.png"));

        playerF.setEstado(Estado.SALTO);
        animacionSaltar(playerF.getImagenPrincipal());
     }
     
     
    public void moverObjeto(KeyEvent e){
        //validar para que no se salga de la pantalla.

        switch (e.getCode()) {
            case UP:
                Gokusaltar();
                break;

            case K:
                ataqueGoku();

                break;

                
            case DOWN:
                Gokucubrirse();

                break;
            case LEFT:    
                moverGokuizquierda();
                
                  break;
                  
            case RIGHT:
                moverGokuderecha();
                
                break;
                  
            case N:
                ataquefinalGoku();
                
               
                break;
  
            default:
                
                player.setEstado(Estado.QUIETO);
                break;
                
            
        }
        


       }
     
    public void moverPlayer2(KeyEvent e){
         switch(e.getCode()){
             case W:
                Billsaltar();
                 break;
                 
             case S:
                Billcubrirse();
                 break;
                 
             case A:
                 moverBillizquierda();
              
                 break;
             case D:
                moverBillderecha();
                 break;
                 
                 
             case G:
                Billataquefinal();
                  
                    //checkCollision();
                
                 break;
             case E:
                 break;
             
         }
     }
     
    public void accionMaquina(String accion){
         switch(accion){
             case "atacar":
                 
                 //caso de que goku se cubra
                 if(playerF.getNumeroAtaque()<5){
                    if(playerF.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()>275){
                        moverBillderechalento();
                    }
                    else{
                    moverBillderecha();
                        
                        
                    }
                 }
                 else{
                 
                     Billataque();}
                 
                 
                 
                 break;
                 
            //goku lanza la genkidama   
             case "saltar":
                 if(playerF.getNumeroAtaque()>5){
                     Billcubrirse();
                     
                     
                 }
                 break;
                 
                 
                 
              //no cambiar   
             case "va_izquierda":
                 if(playerF.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()>375){
                    moverBillizquierda();
                     
                 }
                 else{
                     if(playerF.getNumeroAtaqueFinal()>0&&playerF.getNumeroAtaque()<5){
                         Billataquefinal();
                     }
                     else{
                        moverBillizquierdalento();
                     }
                 }
                 
                 break;
                 
                 
                 
              //no cambiar   
             case "va_derecha":
                 if(playerF.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()<275 && playerF.getImagenPrincipal().getLayoutX()>950){
                    Billataque();
                     
                 }
                 
                 if(playerF.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()>350){
                    if(playerF.getNumeroAtaque()>4){
                    Billataque();}
                    moverBillderechalento();
                     
                 }
                 else{
                    moverBillderecha();
                 }
                
                 break;
                 
            //accion para cuando goku salte   
             case "ESTAR QUIETO":
                 if(playerF.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()<125){
                    Billcubrirse();
                    
                 }
                 else{
                     if(player.getNumeroAtaque()<6){
                        Billsaltar();
                    }
                     
                     else{
                         Billcubrirse();
                    }
                    }
                 
                 break;
                 
                 
             
            //accion que hara bill cuando goku ataque
            case "cubrirse":
            
                if(player.getNumeroAtaque()>4){
                    Billcubrirse();
                }
               


               
                break;
                 
                 
                 
             default:
                 System.out.println("Bills esta quieto");
                 break;
             
             
         }}
         
     
     
    public void inteligenciaMaquina(Estado estado){
         ArbolBinario abb=new ArbolBinario();

        NodoAB n1= new NodoAB(); 
    n1.setEstado(Estado.QUIETO.toString());
    NodoAB n2= new NodoAB();
    n2.setAccion("atacar");
    NodoAB n3= new NodoAB();
    n3.setEstado(Estado.DERECHA.toString());
    NodoAB n4= new NodoAB();
    n4.setAccion("va_izquierda");
    NodoAB n5= new NodoAB();
    n5.setEstado(Estado.IZQUIERDA.toString());
    NodoAB n6= new NodoAB();
    n6.setAccion("va_derecha");
    NodoAB n7= new NodoAB();
    n7.setEstado(Estado.ATAQUE.toString());
    NodoAB n8= new NodoAB();
    n8.setAccion("cubrirse");
    NodoAB n9= new NodoAB();
    n9.setEstado(Estado.ATAQUE_FINAL.toString());
    NodoAB n10= new NodoAB();
    n10.setAccion("saltar");
    NodoAB n11=new NodoAB();
    n11.setEstado(Estado.SALTO.toString());
    NodoAB n12= new NodoAB();
    n12.setAccion("ESTAR QUIETO");
    NodoAB n13=new NodoAB();
    n13.setEstado(Estado.AGACHADO.toString());
    NodoAB n14= new NodoAB();
    n14.setAccion("atacar");
    NodoAB nfinal= new NodoAB();
    nfinal.setEstado(Estado.QUIETO.toString());
    
    //Creacion arbol
    //creacion nodo raiz
    abb.add(null, n1);
    //creacion raiz-izquierda1
     abb.add(n1, n3);
     //creacion raiz-derecha1
     abb.add(n1, n2);
     //creacion izquierda1-izquierda2
     abb.add(n3, n5);
     //creacion izquierda1-derecha2
     abb.add(n3,n6);
     //creacion izquierda2-izquierda3
      abb.add(n5,n7);
      //creacion izquierda2-derecha3
     abb.add(n5, n4);
     //creacion izquierda3-izquierda4
      abb.add(n7,n9);
      //creacion izquierda3-derecha4
     abb.add(n7, n8);
     //creacion izquierda4-izquierda5
     abb.add(n9, n11);
     abb.add(n9,n10);
     abb.add(n11,n13);
     abb.add(n11,n12);
     abb.add(n13,nfinal);
     abb.add(n13,n14);
    //    //recorrer arbol
    //    //------------
            accionMaquina(abb.recorrerarbol(estado.toString()).getAccion());
     }
     
     
     void animacionSaltar(ImageView personaje){
        
        PathTransition transition = new PathTransition();

         Line linea = new Line();
         linea.setStartX(personaje.getX());
         linea.setStartY(personaje.getY()+ (personaje.getFitHeight()/2));
         linea.setEndX(personaje.getX());
         linea.setEndY(personaje.getY()-122);

         transition.setPath(linea);

         transition.setNode(personaje);
         transition.setDuration(Duration.seconds(0.4));
         transition.setCycleCount(2);
         transition.setAutoReverse(true);
         transition.play();
    }
    
    
    
    public BorderPane getRoot(){
        return root;
    }
    
     class Ejecutaanimacion implements Runnable{

            @Override
            public void run() {
                for(int i=0; i<7; i++){
                    Platform.runLater(()->creaNuevoMeteorito());
                    try {
                        
                        Thread.sleep(5500);
                    } catch (InterruptedException ex) {
                        System.out.println("");
                    }




                }}
    }
     
    //metodo cuando 2 personajes se aproximan
     public boolean estanCruzados(){
        return playerF.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()<100;
     }
     
     //Gettters and Stters necesarios

    
}
