/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

/**
 *
 * @author adan
 */
public final class Opciones {
    
private BorderPane root;
    private Button imagen1, imagen2, tiempo1, tiempo2,jugar, extra;
    private Label tiempo, escenario;
    private HBox boximagen,boxtiempo,boxsalir;
    private VBox box;
    private final String ruta1;
    private final String ruta2;
    private final int tiempo_1;
    private final int tiempo_2;
    static int tiempoRestante;
    

    
    Opciones() {
        ruta1 = "imagenes/escenario1.jpg";
        ruta2 = "imagenes/escenario.jpg";
        tiempoRestante = 4;
        tiempo_1=75;
        tiempo_2=50;
        
        
        organizarpanel();
       // Proyecto.ponerMusica("src/musica/inicio.mp3");
    }
    
    public void organizarpanel(){
        root = new BorderPane();
        box = new VBox();
        boximagen = new HBox();
        boxsalir = new HBox();
        boxtiempo = new HBox();
        jugar = new Button();
        tiempo = new Label();
        escenario = new Label();
        extra = new Button();
        
        imagen1 = new Button();
        imagen2 = new Button();
        tiempo1 = new Button();
        tiempo2 = new Button();
        
        
        tiempo.setTextFill(Color.web("#071CB8"));
        tiempo.setFont(Font.font("Cambria", 38));
        tiempo.setText("Seleccione el tiempo:");
        
        
        escenario.setTextFill(Color.web("#071CB8"));
        escenario.setFont(Font.font("Cambria", 38));
        escenario.setText("Seleccione el escenario:");
        
        
        
        
        extra.setStyle("-fx-background-color: transparent;");
        ImageView extra1 = new ImageView("imagenes/BOTONExtra.png");
	extra1.setFitWidth(185);
	extra1.setFitHeight(115);
	extra.setGraphic(extra1);
        
        extra.setOnMouseEntered((MouseEvent e) -> {
            extra.setScaleX(1.1);
            extra.setScaleY(1.1);
        });
 
        extra.setOnMouseExited((MouseEvent e) -> {
            extra.setScaleX(1);
            extra.setScaleY(1);
        });
        
        
        
   
        
        
     
        
        
        
        
        
        imagen1.setStyle("-fx-background-color: transparent; ");
        ImageView image = new ImageView(ruta1);
	image.setFitWidth(275);
	image.setFitHeight(275);
	imagen1.setGraphic(image);
        
        imagen1.setOnMouseEntered((MouseEvent e) -> {
            imagen1.setScaleX(1.1);
            imagen1.setScaleY(1.1);
        });
 
        imagen1.setOnMouseExited((MouseEvent e) -> {
            imagen1.setScaleX(1);
            imagen1.setScaleY(1);
        });
        
        Media sound2 = new Media(new File("src/musica/opciones.mp3").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound2);
        mediaPlayer.play();
        
        
        
        extra.setOnAction(e -> {Proyecto.cambiarVentana(root, new Extra().getRoot());
        mediaPlayer.stop();});
        
        imagen1.setOnAction(e -> {
                                    Proyecto.cambiarVentana(root, new PaneOrganizer(ruta1).getRoot());
                                    mediaPlayer.stop();
                                    
                                    });
        
        imagen2.setOnAction(e -> {Proyecto.cambiarVentana(root, new PaneOrganizer(ruta2).getRoot());mediaPlayer.stop();
                                    mediaPlayer.stop();});
        
        
        imagen2.setStyle("-fx-background-color: transparent; ");
        ImageView image1 = new ImageView(ruta2);
	image1.setFitWidth(275);
	image1.setFitHeight(275);
	imagen2.setGraphic(image1);
        
        imagen2.setOnMouseEntered((MouseEvent e) -> {
            imagen2.setScaleX(1.1);
            imagen2.setScaleY(1.1);
        });
 
        imagen2.setOnMouseExited((MouseEvent e) -> {
            imagen2.setScaleX(1);
            imagen2.setScaleY(1);
        });
        
        
        
        tiempo1.setStyle("-fx-background-color: transparent; ");
        ImageView image2 = new ImageView("imagenes/reloj.gif");
	image2.setFitWidth(125);
	image2.setFitHeight(150);
	tiempo1.setGraphic(image2);
        tiempo1.setText(String.valueOf(tiempo_1));
        tiempo1.setTextFill(Paint.valueOf("white"));
        tiempo1.setFont(Font.font("Cambria", 75));
        
        tiempo1.setOnMouseClicked(e -> {
                                        image2.setOpacity(100);
                                        tiempo1.setTextFill(Paint.valueOf("red"));
                                        tiempo2.setTextFill(Paint.valueOf("white"));
                                        });
        
        tiempo1.setOnMouseEntered((MouseEvent e) -> {
            tiempo1.setScaleX(1.1);
            tiempo1.setScaleY(1.1);
        });
 
        tiempo1.setOnMouseExited((MouseEvent e) -> {
            tiempo1.setScaleX(1);
            tiempo1.setScaleY(1);
        });
        
    
        
        
        
        tiempo2.setStyle("-fx-background-color: transparent; ");
        ImageView image3 = new ImageView("imagenes/reloj.gif");
	image3.setFitWidth(125);
	image3.setFitHeight(150);
	tiempo2.setGraphic(image3);
        tiempo2.setText(String.valueOf(tiempo_2));
        tiempo2.setTextFill(Paint.valueOf("white"));
        tiempo2.setFont(Font.font("Cambria", 75));
        
        tiempo2.setOnMouseClicked(e -> {
                                        image3.setOpacity(20);
                                        tiempo2.setTextFill(Paint.valueOf("red"));
                                        tiempo1.setTextFill(Paint.valueOf("white"));
                                        });
        
        tiempo2.setOnMouseEntered((MouseEvent e) -> {
            tiempo2.setScaleX(1.1);
            tiempo2.setScaleY(1.1);
        });
 
        tiempo2.setOnMouseExited((MouseEvent e) -> {
            tiempo2.setScaleX(1);
            tiempo2.setScaleY(1);
        });
        
        tiempo1.setOnAction(e -> {tiempoRestante = tiempo_1;});
        tiempo2.setOnAction(e -> {tiempoRestante = tiempo_2;});
        
        
        jugar.setStyle("-fx-background-color: transparent; ");
        ImageView image4 = new ImageView("imagenes/arena.gif");
	image4.setFitWidth(175);
	image4.setFitHeight(125);
	jugar.setGraphic(image4);
        
        jugar.setOnMouseEntered((MouseEvent e) -> {
            jugar.setScaleX(1.1);
            jugar.setScaleY(1.1);
        });
 
        jugar.setOnMouseExited((MouseEvent e) -> {
            jugar.setScaleX(1);
            jugar.setScaleY(1);
        });
        
        
       // jugar.setOnAction(e -> {Proyecto.cambiarVentana(root, new PaneOrganizer().getRoot());});
        
        boximagen.getChildren().addAll(imagen1,imagen2);
        boximagen.setSpacing(45);
        boximagen.setAlignment(Pos.CENTER);
        
        boxtiempo.getChildren().addAll(tiempo1,tiempo2);
        boxtiempo.setSpacing(15);
        boxtiempo.setAlignment(Pos.CENTER);
        
        boxsalir.getChildren().add(extra);
        boxsalir.setAlignment(Pos.BOTTOM_RIGHT);
        
        box.getChildren().addAll(boxtiempo,boximagen);
        box.setAlignment(Pos.CENTER);
        
        root.setLeft(box);
        
        root.setRight(boxsalir);
        
  
        
        
        root.setStyle("-fx-background-image: url('/imagenes/prueba0.jpg'); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Pantalla.ANCHO + " " + Pantalla.ALTO + ";");

      //  box.setAlignment(Pos.CENTER);
        box.setSpacing(50);
       // root.setCenter(box);               
        
        
   
    }
  
  
   
    BorderPane getRoot() {
        return root;
    }

    
}


