/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.File;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 *
 * @author adan
 */
public class Extra {
    private BorderPane root;
    private Button inicio, salir, bill, broly,billgif,brolygif;
    private HBox box,box1,boximagen;
    private VBox vbox;
    
    Extra() {
        
        
        organizarpanel();
        
    }
    
    public void organizarpanel(){
        root = new BorderPane();
        box = new HBox();
        box1 = new HBox();
        vbox = new VBox();
        boximagen = new HBox();
        billgif = new Button();
        brolygif = new Button();
        
        inicio = new Button();
      
        salir = new Button();
        bill = new Button();
        broly = new Button();
        
        Media sound2 = new Media(new File("src/musica/theme21.mp3").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound2);
        mediaPlayer.play();
        
        
        
        brolygif.setStyle("-fx-background-color: transparent; ");
        ImageView imagebroly = new ImageView("imagenes/brolypelea.gif");
	imagebroly.setFitWidth(300);
	imagebroly.setFitHeight(300);
	brolygif.setGraphic(imagebroly);
        
        
        
        
        billgif.setStyle("-fx-background-color: transparent; ");
        ImageView bill2 = new ImageView("imagenes/billpelea.gif");
	bill2.setFitWidth(300);
	bill2.setFitHeight(300);
	billgif.setGraphic(bill2);
      

        inicio.setStyle("-fx-background-color: transparent; ");
        ImageView image = new ImageView("imagenes/BOTONMENU.png");
	image.setFitWidth(125);
	image.setFitHeight(125);
	inicio.setGraphic(image);
        
        inicio.setOnMouseEntered((MouseEvent e) -> {
            inicio.setScaleX(1.1);
            inicio.setScaleY(1.1);
        });
 
        inicio.setOnMouseExited((MouseEvent e) -> {
            inicio.setScaleX(1);
            inicio.setScaleY(1);
        });
        
        
        
        bill.setStyle("-fx-background-color: transparent; ");
        ImageView bill1 = new ImageView("imagenes/botonbill.png");
	bill1.setFitWidth(175);
	bill1.setFitHeight(310);
	bill.setGraphic(bill1);
        
        bill.setOnMouseEntered((MouseEvent e) -> {
            boximagen.getChildren().addAll(billgif);
        });
 
        bill.setOnMouseExited((MouseEvent e) -> {
            boximagen.getChildren().removeAll(billgif);
        });
        
        
        
        broly.setStyle("-fx-background-color: transparent; ");
        ImageView broly1 = new ImageView("imagenes/botonbroly.png");
	broly1.setFitWidth(175);
	broly1.setFitHeight(310);
	broly.setGraphic(broly1);
        
        broly.setOnMouseEntered((MouseEvent e) -> {
            boximagen.getChildren().addAll(brolygif);
        });
 
        broly.setOnMouseExited((MouseEvent e) -> {
            boximagen.getChildren().removeAll(brolygif);
        });
        
        boximagen.setAlignment(Pos.CENTER);
        
        
        
   
        
        
     
        
        
        
        inicio.setOnAction(e -> {Proyecto.cambiarVentana(root, new Menu().getRoot());
        mediaPlayer.stop();});
        
        
        salir.setStyle("-fx-background-color: transparent; ");
        ImageView image2 = new ImageView("imagenes/BOTONSalirBrolyGoku.png");
	image2.setFitWidth(125);
	image2.setFitHeight(125);
	salir.setGraphic(image2);
        
           salir.setOnMouseEntered((MouseEvent e) -> {
               salir.setScaleX(1.1);
               salir.setScaleY(1.1);
        });
 
        salir.setOnMouseExited((MouseEvent e) -> {
            salir.setScaleX(1);
            salir.setScaleY(1);
        });
            
        salir.setOnAction(e -> Platform.exit());
        
        
        box.getChildren().addAll(inicio,salir);
        box1.getChildren().addAll(bill,broly);
        
        
        
        root.setStyle("-fx-background-image: url('/imagenes/planeta.jpg'); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Pantalla.ANCHO + " " + Pantalla.ALTO + ";");

        vbox.getChildren().addAll(box,box1);
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        box.setAlignment(Pos.TOP_CENTER);
        box1.setAlignment(Pos.CENTER);
        box1.setSpacing(50);
        box.setSpacing(50);
        root.setTop(vbox); 
        root.setBottom(boximagen);
        
        
       
      
        
       
      
        
        
     
        
    
        
         
     
        
        
        
   
    }
  
  
   
    BorderPane getRoot() {
        return root;
    }

    
    
}
