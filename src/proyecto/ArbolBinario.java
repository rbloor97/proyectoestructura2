package proyecto;


public class ArbolBinario {
    NodoAB raiz;
    public ArbolBinario(){
        raiz=null;
    }

    public void setRaiz(NodoAB raiz) {
        this.raiz = raiz;
    }

    public boolean isEmpty(){
        return raiz==null;
    }
   
    public int altura(){
        return alturarecursivo(this.raiz);
        
    }
    private int alturarecursivo(NodoAB nodo) {
        if(nodo == null){
            return 0;
        }
        else
        {
            return 1 +(Math.max(alturarecursivo(nodo.getDer()),alturarecursivo(nodo.getIzq()))); 
 }
    }
 

    public NodoAB getRaiz() {
        return raiz;
    } 
    public boolean add(NodoAB padre,NodoAB hijo){
        if(isEmpty()&& padre==null && hijo!=null){
           setRaiz(hijo);
        }
        else if(padre!=null && hijo!= null && !isEmpty() && searchNodo(hijo)==null){//searchNodo(element)==null valida si el hijo está en el arbol
            NodoAB ref= searchNodo(padre);
            if(ref==null)//si no está el nodo
                return false;
            if(ref.getIzq()==null){
                ref.setIzq(hijo);
                return true;}
            else if(ref.getDer()==null){
                ref.setDer(hijo);
                return true;
            }
            return false;//quiere decir que el padre ya tiene dos hijos
                
        }
        return false;  
    }
    
        private NodoAB searchNodo(NodoAB nodo){//es un contains todo este metodo para saber si está y si no agregarlo
            return searchNodoRecursivo(nodo,this.raiz);
        }
        private NodoAB  searchNodoRecursivo(NodoAB nodo, NodoAB nodo1){
            if(nodo1==null)
                return null;
            else if(nodo1.equals(nodo))
                return nodo;
            NodoAB i=searchNodoRecursivo(nodo,nodo1.getIzq());
            NodoAB j=searchNodoRecursivo(nodo,nodo1.getDer());
            if(i==null && j!=null)
                return j;
            else if(i!=null && j==null)
                return i;
            return null;
        } 
    public int contarNodo(){
        return contarNodoRecursivo(this.raiz);}
        private int contarNodoRecursivo(NodoAB nodo ){
            if(nodo==null)
                return 0;
            return 1+ contarNodoRecursivo(nodo.getIzq())+ contarNodoRecursivo(nodo.getDer());      
    }
        public void postorden(){
        postorden(this.raiz);
        System.out.println();
    }
    private void postorden(NodoAB nodo){
        if(nodo==null){
        }
        else{
            postorden(nodo.getIzq());
            postorden(nodo.getDer());
            System.out.print(nodo.getEstado()+"");
            
        }
            
    }
public NodoAB recorrerarbol(String Estado){
    return recorrerarbol(Estado,this.raiz);
}
private NodoAB recorrerarbol(String Estado,NodoAB node){
    if(isEmpty() || node==null){
        return null;
    }
    else if(node.getEstado().equals(Estado)){
        return node.getDer();
    }
    else{
       return  recorrerarbol(Estado,node.getIzq());
    }      
}
}



    

