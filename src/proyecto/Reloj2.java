/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Orientation;
import javafx.scene.Camera;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollBar;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

public class Reloj2 {

    //instanciar los datos y funciones que vamos a usar.
    private Button lblReloj,vidavegeta,tiempo,vidafreezer,ataquevegeta,ataquefreezer, ataquefinalvegueta, ataquefinalfreezer;
    private ScrollBar scroll;
    private ImageView im;
    Player vegeta = PaneOrganizer2.player;
    //Freezer freezer = PaneOrganizer.playerF;
    Enemigo2 enemigo2 = PaneOrganizer2.playerF2;
    int hora;
    private int n;
    ImageView imagen = new ImageView("/imagenes/caravegeta.jpg");
    Timeline vida;
    Timeline valores = new Timeline();
    Timeline valores1 = new Timeline();
    Timeline recuperar = new Timeline();



    Reloj2(int tiempoRestante) {
        scroll = new ScrollBar();
        hora = tiempoRestante;
        tiempo = new Button();
        ataquevegeta = new Button();
        ataquefreezer = new Button();
        ataquefinalvegueta = new Button();
        ataquefinalfreezer = new Button();
        
        
        
        ataquevegeta.setTextFill(Color.web("#FAF3F3"));
        ataquevegeta.setFont(Font.font("Cambria", 25));
        ataquevegeta.setStyle("-fx-background-color:  transparent; ");
        ImageView esfera = new ImageView("imagenes/ataque1.png");
        esfera.setFitHeight(95);
        esfera.setFitWidth(95);
        ataquevegeta.setGraphic(esfera);
        ataquevegeta.setText(String.valueOf(vegeta.getNumeroAtaque()));
        
        
        ataquefinalvegueta.setTextFill(Color.web("#FAF3F3"));
        ataquefinalvegueta.setFont(Font.font("Cambria", 25));
        ataquefinalvegueta.setStyle("-fx-background-color:  transparent; ");
        ImageView finalvegeta = new ImageView("imagenes/genki.png");
        finalvegeta.setFitHeight(95);
        finalvegeta.setFitWidth(95);
        ataquefinalvegueta.setGraphic(finalvegeta);
        ataquefinalvegueta.setText(String.valueOf(vegeta.getNumeroAtaquefinal()));
        
        ataquefinalfreezer.setTextFill(Color.web("#FAF3F3"));
        ataquefinalfreezer.setFont(Font.font("Cambria", 25));
        ataquefinalfreezer.setStyle("-fx-background-color:  transparent; ");
        ImageView finalfreezer = new ImageView("imagenes/bolaverde.png");
        finalfreezer.setFitHeight(100);
        finalfreezer.setFitWidth(100);
        ataquefinalfreezer.setGraphic(finalfreezer);
        ataquefinalfreezer.setText(String.valueOf(enemigo2.getNumeroAtaqueFinal()));
        
        
        ataquefreezer.setTextFill(Color.web("#FAF3F3"));
        ataquefreezer.setFont(Font.font("Cambria", 25));
        ataquefreezer.setStyle("-fx-background-color:  transparent; ");
        ImageView esfera1 = new ImageView("imagenes/energia.png");
        esfera1.setFitHeight(95);
        esfera1.setFitWidth(95);
        ataquefreezer.setGraphic(esfera1);
        ataquefreezer.setText(String.valueOf(enemigo2.getNumeroAtaque()));
        
        
        tiempo.setTextFill(Color.web("#FAF3F3"));
        tiempo.setFont(Font.font("Cambria", 25));
        tiempo.setStyle("-fx-background-color:  transparent; ");
        ImageView img0 = new ImageView("imagenes/reloj.gif");
        img0.setFitHeight(95);
        img0.setFitWidth(95);
        tiempo.setGraphic(img0);
        
        
        
   
        //toma los valores del guerrero y los setea en los diferentes string.
        //dinero guerrero.
        
        
        
         vidafreezer = new Button();
         vidafreezer.setText(String.valueOf(enemigo2.getCantidadVida()));
        vidafreezer.setTextFill(Color.web("#FAF3F3"));
        vidafreezer.setFont(Font.font("Cambria", 20));
        vidafreezer.setStyle("-fx-background-color:  transparent; ");
        //imagen de la bolsa de dinero.
        ImageView img1 = new ImageView("imagenes/brolycara.gif");
        img1.setFitHeight(105);
        img1.setFitWidth(105);
        vidafreezer.setGraphic(img1);
        
        
        vidavegeta = new Button();
        vidavegeta.setText(String.valueOf(vegeta.getCantidadVida()));
        vidavegeta.setStyle("-fx-background-color:  transparent; ");
        vidavegeta.setTextFill(Color.web("#FAF3F3"));
        vidavegeta.setFont(Font.font("Cambria", 20));
        ImageView img = new ImageView("imagenes/caragoku.gif");
        img.setFitHeight(105);
        img.setFitWidth(105);
         vidavegeta.setGraphic(img);
        
        
        im = new ImageView("imagenes/inicio.jpeg");
        im.setFitHeight(50);
        img1.setFitWidth(vegeta.getCantidadVida());
       //
        scroll.setOrientation(Orientation.HORIZONTAL);
        scroll.setStyle("#FAF3F3");
        scroll.setValue(vegeta.getCantidadVida());
        

        //muestra la edad que tiene el guerrero.
       
        manejarTiempo();
        
    }

  
//metodo manejarTiempo controla los años de vida con Timeline y keyframe
    void manejarTiempo() {
        //crear los timeline y keyframe a utilizar.
        vida = new Timeline();
        valores = new Timeline();
        valores1 = new Timeline();
        recuperar = new Timeline();

        KeyFrame kfvalores = new KeyFrame(Duration.millis(1), e -> mostrarvalores());
        KeyFrame kfvalores1 = new KeyFrame(Duration.seconds(1), e -> mostrarvalores1());
        KeyFrame kfvalores2 = new KeyFrame(Duration.seconds(3), e -> recuperar());
       

        valores.getKeyFrames().addAll(kfvalores);
        valores1.getKeyFrames().addAll(kfvalores1);
        recuperar.getKeyFrames().addAll(kfvalores2);

        vida.setCycleCount(Timeline.INDEFINITE);
        valores.setCycleCount(Timeline.INDEFINITE);
        recuperar.setCycleCount(Timeline.INDEFINITE);
        valores1.setCycleCount(Timeline.INDEFINITE);

        vida.play();
        valores.play();
        recuperar.play();
        valores1.play();
    }
    
    
    
    
    void recuperar(){
        if (vegeta.getNumeroAtaque()<6){
            vegeta.setNumeroAtaque(vegeta.getNumeroAtaque()+1);
        }
        
        
        if (enemigo2.getNumeroAtaqueFinal()<2){
            enemigo2.setNumeroAtaqueFinal(enemigo2.getNumeroAtaqueFinal()+1);
        }
        
        
        if (enemigo2.getNumeroAtaque()<6){
            enemigo2.setNumeroAtaque(enemigo2.getNumeroAtaque()+1);
        }
        
        if(vegeta.getNumeroAtaquefinal()<1){
            vegeta.setNumeroAtaquefinal(vegeta.getNumeroAtaquefinal()+1);
        }
        
    }

    public Button getAtaquefinalfreezer() {
        return ataquefinalfreezer;
    }

    public void setAtaquefinalfreezer(Button ataquefinalfreezer) {
        this.ataquefinalfreezer = ataquefinalfreezer;
    }

    public Button getAtaquefinalvegueta() {
        return ataquefinalvegueta;
    }

    public void setAtaquefinalvegueta(Button ataquefinalvegueta) {
        this.ataquefinalvegueta = ataquefinalvegueta;
    }
// metodo mostrarvalores setea la alimentacion, animo, edad y limpieza etc de la mascosta 
    void mostrarvalores() {
        tiempo.setText(String.valueOf(hora));
        
        
        
        
       
        
        
        
        ataquefinalvegueta.setText(String.valueOf(vegeta.getNumeroAtaquefinal()));
        
        ataquevegeta.setText(String.valueOf(vegeta.getNumeroAtaque()));
        ataquefreezer.setText(String.valueOf(enemigo2.getNumeroAtaque()));
        
        ataquefinalfreezer.setText(String.valueOf(enemigo2.getNumeroAtaqueFinal()));
        
        vegeta.setCantidadVida(vegeta.getCantidadVida());
        vidavegeta.setText(String.valueOf(vegeta.getCantidadVida()));
        
        im.setFitWidth(vegeta.getCantidadVida());

        
        
        enemigo2.setCantidadVida(enemigo2.getCantidadVida());
        vidafreezer.setText(String.valueOf(enemigo2.getCantidadVida()));
      
        
        
    
    }

    public Button getAtaquefreezer() {
        return ataquefreezer;
    }

    public void setAtaquefreezer(Button ataquefreezer) {
        this.ataquefreezer = ataquefreezer;
    }

    public Button getAtaquevegeta() {
        return ataquevegeta;
    }

    public void setAtaquevegeta(Button ataquevegeta) {
        this.ataquevegeta = ataquevegeta;
    }

    public ScrollBar getScroll() {
        return scroll;
    }

    public void setScroll(ScrollBar scroll) {
        this.scroll = scroll;
    }
    
    void mostrarvalores1() {
        if(hora > 0){
        hora = hora - 1;
        }
        tiempo.setText(String.valueOf(hora));
        
        
    }

    public Button getTiempo() {
        return tiempo;
    }

    public void setTiempo(Button tiempo) {
        this.tiempo = tiempo;
    }

    
    public ImageView getIm() {
        return im;
    }

    public void setIm(ImageView im) {
        this.im = im;
    }
    

    
//getters and setters de todos los datos a necesitar    
    public Button getLblReloj() {
        return lblReloj;
    }

    public Button getSemillas() {
        return vidavegeta;
    }

    public void setSemillas(Button semillas) {
        this.vidavegeta = vidavegeta;
    }


    public Timeline getVida() {
        return vida;
    }

    public void setVida(Timeline vida) {
        this.vida = vida;
    }

    
    public Button getVidafreezer() {
        return vidafreezer;
    }

    public void setVidafreezer(Button vidafreezer) {
        this.vidafreezer = vidafreezer;
    }

    public Button getVidavegeta() {
        return vidavegeta;
    }

    public void setVidavegeta(Button vidavegeta) {
        this.vidavegeta = vidavegeta;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }
    
    



}
