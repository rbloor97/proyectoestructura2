package proyecto;

import java.awt.Rectangle;
import java.util.List;
import java.util.Map;
import javafx.animation.PathTransition;
import javafx.scene.image.ImageView;

public class Player {
   
    //atributos
    private String nombre;
    private int cantidadVida, cantidadAtaque, numeroAtaque, cantidadAtaquefinal, numeroAtaquefinal;
    private List<String> acciones;
    private Map<String, List<ImageView>> sprites; //coleccion para almacenar los sprites del personaje
    private ImageView imagenPrincipal;
    
    private Estado estado;
    private String RUTA_IMAGEN_PRINCIPAL = "imagenes/sprites/Goku/goku1.png";

    public void setRUTA_IMAGEN_PRINCIPAL(String RUTA_IMAGEN_PRINCIPAL) {
        this.RUTA_IMAGEN_PRINCIPAL = RUTA_IMAGEN_PRINCIPAL;
    }
    
    //constructores
    public Player() {
        
        nombre = "";
        cantidadVida = 100;
        cantidadAtaque = 10;
        numeroAtaque = 6;
        numeroAtaquefinal = 1;
        cantidadAtaquefinal = 15;
        estado = Estado.QUIETO;
        imagenPrincipal = new ImageView(RUTA_IMAGEN_PRINCIPAL);
        imagenPrincipal.setFitHeight(245);
        imagenPrincipal.setFitWidth(115);
        imagenPrincipal.setLayoutX(Pantalla.ANCHO/3);
        imagenPrincipal.setLayoutY(Pantalla.ALTO/2);
    
    }

    public Player(String nombre, int cantidadVida, int cantidadAtaque, int numeroAtaque) {
        this.nombre = nombre;
        this.cantidadVida = cantidadVida;
        this.cantidadAtaque = cantidadAtaque;
        this.imagenPrincipal = new ImageView(RUTA_IMAGEN_PRINCIPAL);
        this.numeroAtaque = numeroAtaque;
    
    
    }

    public int getCantidadAtaquefinal() {
        return cantidadAtaquefinal;
    }

    public void setCantidadAtaquefinal(int cantidadAtaquefinal) {
        this.cantidadAtaquefinal = cantidadAtaquefinal;
    }

    public int getNumeroAtaquefinal() {
        return numeroAtaquefinal;
    }

    public void setNumeroAtaquefinal(int numeroAtaquefinal) {
        this.numeroAtaquefinal = numeroAtaquefinal;
    }

    public int getNumeroAtaque() {
        return numeroAtaque;
    }

    public void setNumeroAtaque(int numeroAtaque) {
        this.numeroAtaque = numeroAtaque;
    }
    
    public Estado getEstado() {
        return estado;
    }

    //getters and setters
    public void setEstado(Estado estado) {    
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadVida() {
        return cantidadVida;
    }

    public void setCantidadVida(int cantidadVida) {
        this.cantidadVida = cantidadVida;
    }
    
    public int getCantidadAtaque() {
        return cantidadAtaque;
    }

    public void setCantidadAtaque(int cantidadAtaque) {
        this.cantidadAtaque = cantidadAtaque;
    }

    public Map<String, List<ImageView>> getSprites() {
        return sprites;
    }

    public void setSprites(Map<String, List<ImageView>> sprites) {
        this.sprites = sprites;
    }
    
    public List<String> getAcciones() {
        return acciones;
    }
    
    public void setAcciones(List<String> acciones) {
        this.acciones = acciones;
    }

    public ImageView getImagenPrincipal() {
        return imagenPrincipal;
    }

    public void setImagenPrincipal(ImageView imagenPrincipal) {
        this.imagenPrincipal = imagenPrincipal;
    }
    
    //otros métodos implementados
    public void cargarSprites(){       
    }
    
    @Override
    public String toString(){
        return "\nNombre: "+this.getNombre()+"\nPuntos de Vida: "+this.getCantidadVida()+
                "\nAtaque: "+this.getCantidadAtaque();
    }
    
    //Rectangulos para la colision
    public Rectangle rectanguloPlayer(){
        return new Rectangle((int)imagenPrincipal.getLayoutX(),(int)imagenPrincipal.getLayoutY(),
                            (int)imagenPrincipal.getFitWidth(),(int)imagenPrincipal.getFitHeight());
    }
     
    //colision con freezer
    public boolean collision(Freezer playerF){
        return this.rectanguloPlayer().intersects(playerF.rectanguloPlayerF());
    }
    
    //colision con enemigo2 del paneorganizer2
    public boolean collision(Enemigo2 playerF2){
        return this.rectanguloPlayer().intersects(playerF2.rectanguloEnemigo2());
    }

    void setEstado(String quieto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
