/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import java.io.File;
import java.util.LinkedList;
import java.util.Random;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Line;
import javafx.util.Duration;

/**
 *
 * @author Usuario
 */
public final class PaneOrganizer2 {
    private BorderPane root;
    private Button salir;
    private HBox box;
    static Player player;
    static Enemigo2 playerF2;
    Reloj2 reloj;
    private final String nombreimagen;
    private LinkedList<Meteorito> LBasura;
    private LinkedList<PlayerAtaque> LAtaque;
    private Meteorito m2;
    public static int tiempoRestante;
    private PlayerAtaque esposa, esposa1;
    MediaPlayer mediaPlayer;
    Timeline valores;
    private final Thread ejecuta;
    LinkedList<PlayerAtaque> lista= new LinkedList<>();
    
    public PaneOrganizer2(String nombreimagen){
        this.nombreimagen = nombreimagen;
        organizador();
        
        ejecuta = new Thread(new Ejecutaanimacion());
        ejecuta.start();
        manejarTiempo();
    }
    
    
    
    public void organizador(){   
       LBasura = new LinkedList<>();
       LAtaque = new LinkedList<>();
        player = new Player();
        playerF2 = new Enemigo2();
        root = new BorderPane();
        salir = new Button();
        reloj = new Reloj2(Opciones.tiempoRestante);
        tiempoRestante = reloj.getHora();

        
        Media sound2 = new Media(new File("src/musica/brolypelea15.mp3").toURI().toString());
        mediaPlayer = new MediaPlayer(sound2);
        mediaPlayer.play();
        
        salir.setStyle("-fx-background-color: transparent; ");
        ImageView image2 = new ImageView("/imagenes/salida.png");
	image2.setFitWidth(85);
	image2.setFitHeight(85);
	salir.setGraphic(image2);

        player.getImagenPrincipal().setLayoutX(Pantalla.ANCHO/3);
        player.getImagenPrincipal().setLayoutY(Pantalla.ALTO-245);

        playerF2.getImagenPrincipal().setLayoutX(Pantalla.ANCHO/1.5);
        playerF2.getImagenPrincipal().setLayoutY(Pantalla.ALTO-325);
        
        
        box = new HBox();
        
        box.getChildren().addAll(salir,reloj.getVidavegeta(),reloj.getAtaquevegeta(),reloj.getAtaquefinalvegueta(),reloj.getTiempo(),reloj.getVidafreezer(),reloj.getAtaquefreezer(),reloj.getAtaquefinalfreezer());
        box.setSpacing(15);
        
        root.getChildren().addAll(player.getImagenPrincipal(),playerF2.getImagenPrincipal());
        
        root.setOnKeyPressed(e->{moverObjeto(e);
                                   // moverPlayer2(e);
                                });
        
        root.setStyle("-fx-background-image: url("+nombreimagen+"); "
                + "-fx-background-position: center center; "
                + "-fx-background-repeat: stretch;"
                + "-fx-background-size:" + Pantalla.ANCHO + " " + Pantalla.ALTO + ";");
        root.setTop(box);
        
        //Dando acciones a lso botones
        salir.setOnAction((ActionEvent e) -> {
            ejecuta.stop();
            m2.getTransition().stop();
            m2.getTimer().stop();
            Platform.exit();
       });
        
    }
    
    void manejarTiempo() {
        //crear los timeline y keyframe a utilizar.
        
        valores = new Timeline();
        KeyFrame kfvalores = new KeyFrame(Duration.millis(1), e -> vercampeon());
        valores.getKeyFrames().addAll(kfvalores);
        valores.setCycleCount(Timeline.INDEFINITE);
        valores.play();
      
    }
    
    public void vercampeon(){
        String musica = "src/musica/brolyataque.mp3";
        String imagenbroly = "imagenes/brola.png";
        String imagengoku = "imagenes/gokudios.png";
        String goku = "src/musica/brolygano.mp3";
               
        if(player.getCantidadVida()<1){            
            reloj.valores.stop();
            reloj.valores1.stop();
            reloj.recuperar.stop();
            reloj.vida.stop();
            mediaPlayer.stop();
            valores.stop();
            
            ejecuta.stop();
            m2.getTimer().stop();
            m2.getTransition().stop(); 
            
            
            
            Proyecto.cambiarVentana(root, new Ganador( imagenbroly,musica).getRoot());
            
             
            
            
        }
        
        if(playerF2.getCantidadVida()<1){
            
            reloj.valores.stop();
            reloj.valores1.stop();
            reloj.recuperar.stop();
            reloj.vida.stop();
            mediaPlayer.stop();
            valores.stop();
            
            ejecuta.stop();
            m2.getTimer().stop();
            m2.getTransition().stop(); 
            
             
            Proyecto.cambiarVentana(root, new Ganador(imagengoku,goku).getRoot());
            
        }
        
        
        
        if(reloj.hora==0){
            
            reloj.valores.stop();
            reloj.valores1.stop();
            reloj.recuperar.stop();
            reloj.vida.stop();
            mediaPlayer.stop();
            valores.stop();
            
            ejecuta.stop();


            tiempoRestante = 0;
            
            
            
            if(player.getCantidadVida()>playerF2.getCantidadVida()){
               
                ejecuta.stop();
                m2.getTransition().stop();  
                m2.getTimer().stop();   
                Proyecto.cambiarVentana(root, new Ganador(imagengoku,goku).getRoot());
            
            }
            
            if(player.getCantidadVida()<playerF2.getCantidadVida()){
                
                ejecuta.stop();
                m2.getTransition().stop();  
                m2.getTimer().stop();   
                Proyecto.cambiarVentana(root, new Ganador( imagenbroly,musica).getRoot());
            
            }
            
            
            if(player.getCantidadVida()==playerF2.getCantidadVida()){
               
                ejecuta.stop();
                m2.getTransition().stop();  
                m2.getTimer().stop();   
                Proyecto.cambiarVentana(root, new Ganador( imagenbroly,musica).getRoot());
            
            }
            
            
            
        }
        
     }
    
    public void creaNuevoMeteorito(){
       // Dinero m2 = new Dinero();
        m2 = new Meteorito(player,playerF2);
        Random num = new Random();
        int rand = num.nextInt(15);
        
        LBasura.add(m2);     
        
        root.getChildren().add(m2.getPane());
        }
      

    void remuevePane(MouseEvent a){
        boolean remove = root.getChildren().remove(a.getSource());

        }
    
    public void checkCollision() {
          
        if (player.collision(playerF2)){          
            //System.out.println("CHOQUE GOKU CONTRA BILLS DETECTADO");
            
        }      
       }
        //return a.getBoundsInParent().intersects(b.getBoundsInParent());
    
    public void moverGokuderecha(){
         
         if(player.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO<=Pantalla.ANCHO-100){
                    
            player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/derecha.png"));
          //  playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/bill.png"));


            player.getImagenPrincipal().setLayoutX(player.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO);
         //   playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX());
            player.setEstado(Estado.DERECHA);

            inteligenciaMaquina(player.getEstado());
            }
                
         
     }
     
     
    public void Gokusaltar(){
        
        player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/salto.png"));  
 
        player.setEstado(Estado.SALTO);
        animacionSaltar(player.getImagenPrincipal());
        inteligenciaMaquina(player.getEstado());
     }
     
    public void moverGokuizquierda(){
         
         if(player.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO>=45){
                   
            player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/retro.png"));  
         

            player.getImagenPrincipal().setLayoutX(player.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO);
           // playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX());
             }
             player.setEstado(Estado.IZQUIERDA);
             checkCollision();
             inteligenciaMaquina(player.getEstado());
         
     }
     
     
    public void ataqueGoku(){
         if(player.getNumeroAtaque()>0){
        esposa = new PlayerAtaque(player,playerF2);



           player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/ataque.png")); 
          // playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/bill.png"));
           player.setEstado(Estado.ATAQUE);
           inteligenciaMaquina(player.getEstado());
            root.getChildren().add(esposa.getAtaqueGoku());
            lista.add(esposa);
            for(PlayerAtaque i: lista){
                i.animacion2(player.getImagenPrincipal().getLayoutY()+40).setOnFinished(ev->{root.getChildren().removeAll(i.getAtaqueGoku());});
            }
            player.setNumeroAtaque(player.getNumeroAtaque()-1); 
        }  
         
     }
     
    public void Gokucubrirse(){
         
         
        player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/cubrir.png"));
     
        if (player.collision(playerF2)){
            player.getImagenPrincipal().setLayoutX(player.getImagenPrincipal().getLayoutX()-10);
        //    playerF.getImagenPrincipal().setLayoutX(playerF.getImagenPrincipal().getLayoutX()+10);
        }

        player.setEstado(Estado.AGACHADO);
        checkCollision();
        inteligenciaMaquina(player.getEstado());
         
     }
     
     
     public void ataquefinalGoku(){
         
          if(player.getNumeroAtaquefinal()>0){

                Media sound1 = new Media(new File("src/musica/genkidama1.mp3").toURI().toString());
                MediaPlayer mediaPlayer1 = new MediaPlayer(sound1);
                mediaPlayer1.play();

                player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/genkidama2.gif"));
                esposa1 = new PlayerAtaque(player,playerF2);

                //playerF.setCantidadVida(playerF.getCantidadVida()-player.getCantidadAtaquefinal());
                lista.add(esposa1);
                root.getChildren().add(esposa1.getGenkidama());
                for(PlayerAtaque i: lista){
                    
                     i.genkidama2(esposa1.getGenkidama()).setOnFinished(ev->{root.getChildren().removeAll(i.getGenkidama());});
                }

                player.setNumeroAtaquefinal(player.getNumeroAtaquefinal()-1); 

            player.setEstado(Estado.ATAQUE_FINAL);
            inteligenciaMaquina(player.getEstado());

        } 
         
     }
     
     
    public void moverBrolyizquierda(){
         
         if(playerF2.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO+5>=35){       
           
            playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/Img6.png"));

            playerF2.getImagenPrincipal().setLayoutX(playerF2.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO-6);
        
            playerF2.setEstado(Estado.IZQUIERDA);
       
         }
    }
         
     
         
    public void moverBrolyizquierdalento(){
         
         if(playerF2.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO+5>=35){       
           
            playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/Img6.png"));

            playerF2.getImagenPrincipal().setLayoutX(playerF2.getImagenPrincipal().getLayoutX()-Pantalla.MOVIMIENTO-2);
        
            playerF2.setEstado(Estado.IZQUIERDA);
       
         }
         
     }
    
     public void moverBrolyderecha(){
         
         if(playerF2.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO-5<=Pantalla.ANCHO-195){                             
          
            playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/Img13.png"));
            playerF2.getImagenPrincipal().setLayoutX(playerF2.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO+6);
          
        }
        playerF2.setEstado(Estado.DERECHA);
        
         
     }
     
      public void moverBrolyderechalento(){
         
         if(playerF2.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO-5<=Pantalla.ANCHO-195){                             
          
            playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/Img13.png"));
            playerF2.getImagenPrincipal().setLayoutX(playerF2.getImagenPrincipal().getLayoutX()+Pantalla.MOVIMIENTO+2);
          
        }
        playerF2.setEstado(Estado.DERECHA);
        
         
     }
     
     
    public void Brolycubrirse(){
       
        playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/Img3.png"));
        if (player.collision(playerF2)){
            player.getImagenPrincipal().setLayoutX(player.getImagenPrincipal().getLayoutX()-10);
            playerF2.getImagenPrincipal().setLayoutX(playerF2.getImagenPrincipal().getLayoutX()+10);
        }

        playerF2.setEstado(Estado.AGACHADO);
               
         
     }
    
    public void Brolyataque(){
        
         if(playerF2.getNumeroAtaque()>0){
             
             
            esposa = new PlayerAtaque(player,playerF2);
            playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/Img4.png"));
            playerF2.setEstado(Estado.ATAQUE);
            
            lista.add(esposa);
            root.getChildren().add(esposa.getAtaqueBroly());
            for(PlayerAtaque i: lista){
               i.animacionAtqEnemigo2(playerF2.getImagenPrincipal().getLayoutY()+55).setOnFinished(ev->{root.getChildren().removeAll(i.getAtaqueBroly());});
            }
            playerF2.setNumeroAtaque(playerF2.getNumeroAtaque()-1); 
            
        
                    
         }
         
        
         
     }
    
    
    public void Brolyataquefinal(){
        
        
        if(playerF2.getNumeroAtaqueFinal()>0){

                Media sound1 = new Media(new File("src/musica/brolyataque.mp3").toURI().toString());
                MediaPlayer mediaPlayer1 = new MediaPlayer(sound1);
                mediaPlayer1.play();

                playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/brolyfinal.gif"));
                esposa = new PlayerAtaque(player,playerF2);
                

                //playerF.setCantidadVida(playerF.getCantidadVida()-player.getCantidadAtaquefinal());
                lista.add(esposa);
                
                root.getChildren().add(esposa.getAtaqueFinalBroly());
                
                for(PlayerAtaque i: lista){
                    
                     i.Destruccion(esposa.getAtaqueFinalBroly()).setOnFinished(ev->{root.getChildren().removeAll(i.getAtaqueFinalBroly());});
                }

                playerF2.setNumeroAtaqueFinal(playerF2.getNumeroAtaqueFinal()-1); 

    
        } 
         
     }
     
    public void Brolysaltar(){
        
        playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/Img2.png"));

       // playerF2.setEstado(Estado.SALTO);
        animacionSaltar(playerF2.getImagenPrincipal());
     }
     
     
     public void moverObjeto(KeyEvent e){
        //validar para que no se salga de la pantalla.

        switch (e.getCode()) {
            case UP:
                Gokusaltar();
              //  Brolysaltar();
                break;

            case K:
                ataqueGoku();
              //  Brolyataque();

                break;

                
            case DOWN:
                Gokucubrirse();
             //   Brolycubrirse();

                break;
            case LEFT:    
                moverGokuizquierda();
               // moverBrolyizquierda();
                
                  break;
                  
            case RIGHT:
                moverGokuderecha();
                
                
                break;
            case A:
                moverBrolyizquierda();
                break;
            case U:
                Brolyataque();
                break;
                
            case D:
                moverBrolyderecha();
               break;
            case S:
                Brolycubrirse();
                break;
            case W:
                Brolysaltar();
                break;
            case M:
                Brolyataquefinal();
                break;
                  
            case N:
                ataquefinalGoku();
             //   Brolyataquefinal();
                
               
                break;
  
            default:
                
                player.setEstado(Estado.QUIETO);
                break;   
        }
       }
     
     public void accionMaquina(String accion){
         switch(accion){
             case "atacar":
                 Brolyataque();
                 
                 break;
                 
             //cuando goku se cubre    
             case "saltar":
                 if(playerF2.getNumeroAtaque()>4){
                     Brolyataque();
                 }
                 else{
                     if(playerF2.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()>375){
                         if(playerF2.getNumeroAtaqueFinal()>1){
                            Brolyataquefinal();}
                         else{
                             Brolycubrirse();
                         }
                    }
                     else{
                         moverBrolyderecha();
                     }
                 }
                 break;
            //cuando goku se mueve a la izquierda broly hara:
             case "va_izquierda":
                 
                 if(playerF2.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()>325){
                     moverBrolyizquierda();
                     
                 }
                 else{
                     moverBrolyizquierdalento();
                 }
                 break;
                 
             //cuando goku se va a la derecha    
             case "va_derecha":
                 if(playerF2.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()>350){
                     moverBrolyderechalento();
                 }
                 else{
                     moverBrolyderecha();
                 }
                 break;
                 
             //cuando goku salta
                 
                 
             case "estar quieto":
                 if(playerF2.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()>350){
                     if(playerF2.getNumeroAtaqueFinal()>1){
                         Brolyataquefinal();
                     }
                     else{
                         Brolycubrirse();
                     }}
                else{
                     if(playerF2.getNumeroAtaque()>5){
                         Brolyataque();
                     }
                     else{
                         Brolysaltar();
                     }
                }
                 break;
             default:
                 playerF2.setEstado(Estado.QUIETO);
                 System.out.println("Broly esta quieto");
                 break;
         }
         
     }
     
      public void inteligenciaMaquina(Estado estado){
         ArbolBinario abb=new ArbolBinario();
NodoAB estadoquieto= new NodoAB(); 
    estadoquieto.setEstado(Estado.QUIETO.toString());
    NodoAB n2= new NodoAB();
    n2.setAccion("atacar");
    NodoAB estadoderecha= new NodoAB();
    estadoderecha.setEstado(Estado.DERECHA.toString());
    NodoAB accionizq= new NodoAB();
    accionizq.setAccion("va_izquierda");
    NodoAB estadoizquierda= new NodoAB();
    estadoizquierda.setEstado(Estado.IZQUIERDA.toString());
    NodoAB accionder= new NodoAB();
    accionder.setAccion("va_derecha");
    NodoAB estadoataque= new NodoAB();
    estadoataque.setEstado(Estado.ATAQUE.toString());
    NodoAB accionagacharse= new NodoAB();
    accionagacharse.setAccion("agacharse");
    NodoAB estadoataquefinal= new NodoAB();
    estadoataquefinal.setEstado(Estado.ATAQUE_FINAL.toString());
    NodoAB estadosalto=new NodoAB();
    estadosalto.setEstado(Estado.SALTO.toString());
    NodoAB accionquieto= new NodoAB();
    accionquieto.setAccion("estar quieto");
    NodoAB estadoagachado=new NodoAB();
    estadoagachado.setEstado(Estado.AGACHADO.toString());
    NodoAB accionatacar= new NodoAB();
    accionatacar.setAccion("atacar");
    NodoAB estadoquietofinal= new NodoAB();
    estadoquietofinal.setEstado(Estado.QUIETO.toString());
    NodoAB estarquieto=new NodoAB();
    estarquieto.setAccion("estar quieto");
    NodoAB estarquieto1=new NodoAB();
    estarquieto1.setAccion("estar quieto");
    NodoAB estarquieto2=new NodoAB();
    estarquieto2.setAccion("estar quieto");
    NodoAB acciosaltar= new NodoAB();
    acciosaltar.setAccion("saltar");
    
    
   abb.add(null, estadoquieto);
    //creacion raiz-izquierda1
     abb.add(estadoquieto, estadoderecha);
     //creacion raiz-derecha1
     //Goku Salta, Broly izq
     abb.add(estadoquieto,accionatacar);
     abb.add(estadoderecha, estadoizquierda);
     //creacion izquierda1-derecha2
     abb.add(estadoderecha,accionder);
     //Goku derecha, Broly Derecha
      abb.add(estadoizquierda,estadoataque);
      //creacion izquierda2-derecha3
     abb.add(estadoizquierda, accionizq);
     //Goku izq a Broly Izq
      abb.add(estadoataque,estadoataquefinal);
      //creacion izquierda3-derecha4
     abb.add(estadoataque, accionagacharse);
     //creacion izquierda4-izquierda5
     abb.add(estadoataquefinal,estadosalto);
     abb.add(estadoataquefinal,estarquieto2);
     // goku ataque fuerte cubrirse
     abb.add(estadosalto,estadoagachado);
     abb.add(estadosalto,estarquieto);
     abb.add(estadoagachado,estadoquietofinal);
     abb.add(estadoagachado,acciosaltar);
   
//    //------------
        accionMaquina(abb.recorrerarbol(estado.toString()).getAccion());
     }
      public void InteligenciaArtificialBroly(Estado estado){
          ArbolBinario abb=new ArbolBinario();
    NodoAB estadoquieto= new NodoAB(); 
    estadoquieto.setEstado(Estado.QUIETO.toString());
    NodoAB n2= new NodoAB();
    n2.setAccion("atacar");
    NodoAB estadoderecha= new NodoAB();
    estadoderecha.setEstado(Estado.DERECHA.toString());
    NodoAB accionizq= new NodoAB();
    accionizq.setAccion("va_izquierda");
    NodoAB estadoizquierda= new NodoAB();
    estadoizquierda.setEstado(Estado.IZQUIERDA.toString());
    NodoAB accionder= new NodoAB();
    accionder.setAccion("va_derecha");
    NodoAB estadoataque= new NodoAB();
    estadoataque.setEstado(Estado.ATAQUE.toString());
    NodoAB accionagacharse= new NodoAB();
    accionagacharse.setAccion("agacharse");
    NodoAB estadoataquefinal= new NodoAB();
    estadoataquefinal.setEstado(Estado.ATAQUE_FINAL.toString());
    NodoAB estadosalto=new NodoAB();
    estadosalto.setEstado(Estado.SALTO.toString());
    NodoAB accionquieto= new NodoAB();
    accionquieto.setAccion("estar quieto");
    NodoAB estadoagachado=new NodoAB();
    estadoagachado.setEstado(Estado.AGACHADO.toString());
    NodoAB accionatacar= new NodoAB();
    accionatacar.setAccion("atacar");
    NodoAB estadoquietofinal= new NodoAB();
    estadoquietofinal.setEstado(Estado.QUIETO.toString());
    NodoAB estarquieto=new NodoAB();
    estarquieto.setAccion("estar quieto");
    NodoAB estarquieto1=new NodoAB();
    estarquieto1.setAccion("estar quieto");
// Si goku salta broly se mueve a la izqierda
//Si goku a la izqierda. Broly a la izqierda
//Goku a la derecha. Broly a la derecha.
//Goku ataca normal si tiene mas de 3 esferas broly se cubre si no no hace nada
//Goku ataq final si el tiempo es mayor a 25 se defiende si no no hace nada
//Goku a la derecha y broly mas de 4 ataqes broly ataca si no hace lo otro q te dije.
//Goku a la izquierda y el tiempo es menor a 35 broly ataca final.
    //Creacion arbol
    //creacion nodo raiz
    abb.add(null, estadoquieto);
    //creacion raiz-izquierda1
     abb.add(estadoquieto, estadoderecha);
     //creacion raiz-derecha1
     //Goku Salta, Broly izq
     abb.add(estadoquieto,accionatacar);
     abb.add(estadoderecha, estadoizquierda);
     //creacion izquierda1-derecha2
     abb.add(estadoderecha,accionder);
     //Goku derecha, Broly Derecha
      abb.add(estadoizquierda,estadoataque);
      //creacion izquierda2-derecha3
     abb.add(estadoizquierda, accionizq);
     //Goku izq a Broly Izq
      abb.add(estadoataque,estadoataquefinal);
      //creacion izquierda3-derecha4
     abb.add(estadoataque, accionquieto);
     //creacion izquierda4-izquierda5
     abb.add(estadoataquefinal,estadosalto);
     abb.add(estadoataquefinal,accionagacharse);
     // goku ataque fuerte cubrirse
     abb.add(estadosalto,estadoagachado);
     abb.add(estadosalto,estarquieto);
     abb.add(estadoagachado,estadoquietofinal);
     abb.add(estadoagachado,estarquieto1);
   
    //    //recorrer arbol
    //    //------------
            accionMaquina(abb.recorrerarbol(estado.toString()).getAccion());
      }
     
     void animacionSaltar(ImageView personaje){
        
    PathTransition transition = new PathTransition();

     Line linea = new Line();
     linea.setStartX(personaje.getX());
     linea.setStartY(personaje.getY()+ (personaje.getFitHeight()/2));
     linea.setEndX(personaje.getX());
     linea.setEndY(personaje.getY()-122);
     
     transition.setPath(linea);
     
     transition.setNode(personaje);
     transition.setDuration(Duration.seconds(0.4));
     transition.setCycleCount(2);
     transition.setAutoReverse(true);
     transition.play();
    }
    
    
    
    public BorderPane getRoot(){
        return root;
    }
    
    
    
    
     class Ejecutaanimacion implements Runnable{

            @Override
            public void run() {
                for(int i=0; i<5; i++){
                    Platform.runLater(()->creaNuevoMeteorito());
                    try {
                        
                        Thread.sleep(5500);
                    } catch (InterruptedException ex) {
                        System.out.println("");
                    }




                }}
    }
     
    //metodo cuando 2 personajes se aproximan
     public boolean estanCruzados(){
        if(playerF2.getImagenPrincipal().getLayoutX()-player.getImagenPrincipal().getLayoutX()<100){
            player.setCantidadVida(player.getCantidadVida()-10);
             return true;
        }
        return false;
     }
    
    
     
     //Gettters and Stters necesarios

     
}
