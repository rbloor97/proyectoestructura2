/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

/**
 *
 * @author adan
 */
import javax.swing.Timer;
import java.awt.event.ActionListener;
import java.util.Random;
import javafx.animation.PathTransition;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Line;
import javafx.util.Duration;
import static proyecto.PaneOrganizer.player;

public class PlayerAtaque {
Player policia;
private ImageView ataqueBill, ataqueFinalBill,ataqueBroly,ataqueFinalBroly,ataqueGoku,genkidama;

    
   
private static final String billataque="/imagenes/hakai.png";
private static final String billataquefinal="/imagenes/energia.png"; 

private static final String brolyataque="/imagenes/billsalir.png";
private static final String brolyataquefinal="/imagenes/bolaverde.png"; 

private static final String gokuataque="/imagenes/kameha.png"; 
private static final String gokugenkidama="/imagenes/ataque1.png"; 
private Line line;
private PathTransition transition;
private Freezer freezer;
private Player player;
private Freezer playerF;
private Enemigo2 playerF2;
private Timer timer;

    PlayerAtaque(Player player,Freezer playerF){   
        inicializarElementos();
        this.player=player;
        this.playerF= PaneOrganizer.playerF;
    
        
    }

    
    //constructor para paneorganizer2
    PlayerAtaque(Player player,Enemigo2 playerF2){   
        inicializarElementos();
        this.player=player;
        this.playerF2=PaneOrganizer2.playerF2;
    
        
    }

    public void inicializarElementos(){
        ataqueBill= new ImageView(new Image(billataque));
        ataqueBill.setFitHeight(55);
        ataqueBill.setFitWidth(55);
        
        ataqueFinalBill= new ImageView(new Image(billataquefinal));
        ataqueFinalBill.setFitHeight(205);
        ataqueFinalBill.setFitWidth(205);
        
        ataqueBroly= new ImageView(new Image(billataque));
        ataqueBroly.setFitHeight(55);
        ataqueBroly.setFitWidth(55);
        
        
        ataqueFinalBroly= new ImageView(new Image(brolyataquefinal));
        ataqueFinalBroly.setFitHeight(205);
        ataqueFinalBroly.setFitWidth(205);
        
        
        
        ataqueGoku= new ImageView(new Image(gokuataque));
        ataqueGoku.setFitHeight(55);
        ataqueGoku.setFitWidth(55);
        
        
        genkidama= new ImageView(new Image(gokugenkidama));
        genkidama.setFitHeight(205);
        genkidama.setFitWidth(205);
        
        
       
        
        freezer = PaneOrganizer.playerF;
        line = new Line();
        policia = new Player();
        
        
       
        
    }
    
  

   
    
    
    PathTransition animacion(double x){
         //1.- Creo un objeto tipo path trasition
         transition = new PathTransition();
         //2.- Seteo el nodo que quiero que se mueva
         transition.setNode(ataqueGoku);
         //3.- Selecciono la forma en que el nodo se va a mover
         transition.setPath(parametrizarLinea(x));         
         //La duracion de la transicion
         transition.setDuration(Duration.seconds(0.8));
         //Cuantas veces se va a repetir la animacion
         transition.setCycleCount(1);
         //Ejecutar la animacion
         transition.setAutoReverse(false);
         timer = new Timer (1000, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE ESFERA");
               checkCollision();
        
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
        transition.play();
         return transition;
    }


    public Line parametrizarLinea(double x){
        
        line.setStartX(PaneOrganizer.player.getImagenPrincipal().getLayoutX()+115);
        line.setEndX(freezer.getImagenPrincipal().getLayoutX()+5);
        line.setEndY(x);
        line.setStartY(x);        
        return line; 

    }
    
    //ANIMACION PARA ESFERA DE ENERGIA DE BILLS
    PathTransition animacionAtqEnemigo1(double x){
         //1.- Creo un objeto tipo path trasition
         transition = new PathTransition();
         //2.- Seteo el nodo que quiero que se mueva
         transition.setNode(ataqueBill);
         //3.- Selecciono la forma en que el nodo se va a mover
         transition.setPath(parametrizarLineaEnem1(x));         
         //La duracion de la transicion
         transition.setDuration(Duration.seconds(0.8));
         //Cuantas veces se va a repetir la animacion
         transition.setCycleCount(1);
         //Ejecutar la animacion
         transition.setAutoReverse(false);
         timer = new Timer (1000, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE ESFERA");
               checkCollision();
        
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
        transition.play();
         return transition;
    }
    
    public Line parametrizarLineaEnem1(double x){
        line.setStartX(freezer.getImagenPrincipal().getLayoutX());
        line.setEndX(PaneOrganizer.player.getImagenPrincipal().getLayoutX()+80);
        line.setEndY(x);
        line.setStartY(x);        
        return line; 

    }
    
    //genkidama hecha por goku
    public PathTransition genkidama(Node n){
         transition = new PathTransition();
        transition.setNode(n);
 
  
        Random r = new Random();
        
     //   int num = r.nextInt((int) 1000);
        Line linea = new Line();
        linea.setStartX(player.getImagenPrincipal().getLayoutX()+30);
        linea.setStartY(player.getImagenPrincipal().getLayoutY()-155);
        linea.setEndX(freezer.getImagenPrincipal().getLayoutX());
        linea.setEndY(freezer.getImagenPrincipal().getLayoutY()+50);
        
        transition.setPath(linea);
        
        transition.setDuration(Duration.seconds(1));
  
        transition.setCycleCount(1);
  
        transition.setAutoReverse(false);     
        
         timer = new Timer (500, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE ESFERAS");
               checkCollisionGenki();
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
      
        transition.play();
        
        return transition;
        
    }
    
    //hakai ataque final de bill
    public PathTransition Hakai(Node n){
        transition = new PathTransition();
        transition.setNode(n);
 
  
        Random r = new Random();
        
     //   int num = r.nextInt((int) 1000);
        Line linea = new Line();
        linea.setStartX(playerF.getImagenPrincipal().getLayoutX()-30);
        linea.setStartY(playerF.getImagenPrincipal().getLayoutY()-155);
        linea.setEndX(player.getImagenPrincipal().getLayoutX()+75);
        linea.setEndY(player.getImagenPrincipal().getLayoutY()+50);
        
        transition.setPath(linea);
        
        transition.setDuration(Duration.seconds(1));
  
        transition.setCycleCount(1);
  
        transition.setAutoReverse(false);     
        
         timer = new Timer (500, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE ESFERAS");
               checkCollisionGenki();
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
      
        transition.play();
        
        return transition;
        
    }
    
    //animaciones para paneOrganizer2
    //esfera de goku en paneorganizer2
    
    
    
    
    PathTransition animacion2(double x){
         //1.- Creo un objeto tipo path trasition
         transition = new PathTransition();
         //2.- Seteo el nodo que quiero que se mueva
         transition.setNode(ataqueGoku);
         //3.- Selecciono la forma en que el nodo se va a mover
         transition.setPath(parametrizarLinea2(x));         
         //La duracion de la transicion
         transition.setDuration(Duration.seconds(0.8));
         //Cuantas veces se va a repetir la animacion
         transition.setCycleCount(1);
         //Ejecutar la animacion
         transition.setAutoReverse(false);
         timer = new Timer (1000, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE ESFERA");
               checkCollision2();
        
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
        transition.play();
         return transition;
    }


    public Line parametrizarLinea2(double x){
        
        line.setStartX(PaneOrganizer2.player.getImagenPrincipal().getLayoutX()+115);
        line.setEndX(playerF2.getImagenPrincipal().getLayoutX());
        line.setEndY(x);
        line.setStartY(x);        
        return line; 

    }
    
    //esfera de ataque del enemigo2 en paneorganizer2
    PathTransition animacionAtqEnemigo2(double n){
         //1.- Creo un objeto tipo path trasition
         transition = new PathTransition();
         //2.- Seteo el nodo que quiero que se mueva
         transition.setNode(ataqueBroly);
         
        Line linea = new Line();
        
        linea.setStartX(PaneOrganizer2.playerF2.getImagenPrincipal().getLayoutX());
        linea.setEndX(PaneOrganizer2.player.getImagenPrincipal().getLayoutX());
        linea.setEndY(PaneOrganizer2.playerF2.getImagenPrincipal().getLayoutY()+115);
        linea.setStartY(PaneOrganizer2.playerF2.getImagenPrincipal().getLayoutY()+115);        
        
         //3.- Selecciono la forma en que el nodo se va a mover
         transition.setPath(linea);         
         //La duracion de la transicion
         transition.setDuration(Duration.seconds(0.8));
         //Cuantas veces se va a repetir la animacion
         transition.setCycleCount(1);
         //Ejecutar la animacion
         transition.setAutoReverse(false);
         timer = new Timer (1000, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE ESFERA");
               checkCollision2();
        
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
        transition.play();
         return transition;
    }
    
    public Line parametrizarLineaEnem2(double x){
        
        line.setStartX(playerF2.getImagenPrincipal().getLayoutX()-10);
        line.setEndX(player.getImagenPrincipal().getLayoutX());
        line.setEndY(x);
        line.setStartY(x);        
        return line; 

    }
    
    //ataque final de broly
    public PathTransition Destruccion(Node n){
        transition = new PathTransition();
        transition.setNode(n);
        Line linea = new Line();
        linea.setStartX(playerF2.getImagenPrincipal().getLayoutX()-25);
        linea.setStartY(playerF2.getImagenPrincipal().getLayoutY()+220);
        linea.setEndX(player.getImagenPrincipal().getLayoutX()+75);
        linea.setEndY(player.getImagenPrincipal().getLayoutY()+45);
        
        transition.setPath(linea);
        
        transition.setDuration(Duration.seconds(1));
  
        transition.setCycleCount(1);
  
        transition.setAutoReverse(false);     
        
         timer = new Timer (500, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE ESFERAS");
             //  checkCollisionGenki();
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
      
        transition.play();
        
        return transition;
        
    }
    
    //genkidama de goku en paneorganizer2
    public PathTransition genkidama2(Node n){
        transition = new PathTransition();
        transition.setNode(n);
        Line linea = new Line();
        linea.setStartX(player.getImagenPrincipal().getLayoutX()+30);
        linea.setStartY(player.getImagenPrincipal().getLayoutY()-155);
        linea.setEndX(playerF2.getImagenPrincipal().getLayoutX());
        linea.setEndY(playerF2.getImagenPrincipal().getLayoutY()+50);
        
        transition.setPath(linea);
        
        transition.setDuration(Duration.seconds(1));
  
        transition.setCycleCount(1);
  
        transition.setAutoReverse(false);     
        
         timer = new Timer (500, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE ESFERAS");
               checkCollisionGenki2();
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
      
        transition.play();
        
        return transition;
        
    }
    
    public PathTransition animacionfinal(Node n) {
        transition = new PathTransition();
        transition.setNode(n);
 
  
        Random r = new Random();
        
        int num = r.nextInt((int) 1000);
        Line linea = new Line();
        linea.setStartX(num+75);
        linea.setStartY(0);
        linea.setEndX(num+75);
        linea.setEndY(Pantalla.ALTO);
        
        transition.setPath(linea);
        
        transition.setDuration(Duration.seconds(2));
  
        transition.setCycleCount(1);
  
        transition.setAutoReverse(false);     
        
         timer = new Timer (500, new ActionListener () 
        {           
           @Override
           public void actionPerformed(java.awt.event.ActionEvent ae) {
               //System.out.println("COLISION EN RELOJ DE ESFERAS");
               checkCollision();
               //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
           }

        }); 
        timer.start();
        System.out.println("Se ejecuta animacion final");
        transition.play();
        
        return transition;
    }

  
   
    
    public Line getLine() {
        return line;
    }
    
    public PathTransition getTransition() {
        return transition;
    }
    
    public Timer getTimer() {
        return timer;
    }
    
    //colisiones
    public void checkCollision(){
        if(this.collision(playerF)){
            //System.out.println("CHOQUE ESFERA CONTRA BILLS DETECTADO");
            timer.stop();
            if(playerF.getCantidadVida()>0 && playerF.getEstado().equals(Estado.AGACHADO)){
                PaneOrganizer.playerF.setCantidadVida(PaneOrganizer.playerF.getCantidadVida()-2);  
               // playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/herido.png"));
            }
            
            else if(playerF.getCantidadVida()>0 && playerF.getEstado().equals(Estado.SALTO)){
               PaneOrganizer.playerF.setCantidadVida(PaneOrganizer.playerF.getCantidadVida()-0); 
            }
            else if(playerF.getCantidadVida()>0 && !playerF.getEstado().equals(Estado.AGACHADO)
                    && !playerF.getEstado().equals(Estado.SALTO)){
                PaneOrganizer.playerF.setCantidadVida(PaneOrganizer.playerF.getCantidadVida()-10);
                PaneOrganizer.playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/herido.png"));
            }
            
            
                //playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/herido.png"));
            }            
        
        else if(this.collision(player)){
            //System.out.println("CHOQUE ESFERA CONTRA PLAYER DETECTADO");
            timer.stop();
            if(player.getCantidadVida()>0 && player.getEstado().equals(Estado.AGACHADO)){
                player.setCantidadVida(player.getCantidadVida()-5);  
                //player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/herido.png"));
            }
            else if(player.getCantidadVida()>0 && player.getEstado().equals(Estado.SALTO)){
                player.setCantidadVida(player.getCantidadVida()-0);  
               // player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/herido.png"));
            } 
             else if(player.getCantidadVida()>0 && !player.getEstado().equals(Estado.AGACHADO) 
                     && !player.getEstado().equals(Estado.SALTO)){
                player.setCantidadVida(player.getCantidadVida()-10);  
                player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/herido.png"));
            } 
        }

    }
    
    //revisando colision de genkidama
    public void checkCollisionGenki(){
       if(this.collisionGenki(playerF)){
            //System.out.println("CHOQUE GENKIDAMA CONTRA BILLS DETECTADO");
            timer.stop();
            if(playerF.getCantidadVida()>0 && playerF.getEstado().equals(Estado.AGACHADO)){
                playerF.setCantidadVida(playerF.getCantidadVida()-5);  
              //  playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/herido.png"));
            }
            else if(playerF.getCantidadVida()>0&& !playerF.getEstado().equals(Estado.AGACHADO)
                    && !playerF.getEstado().equals(Estado.SALTO)){
                playerF.setCantidadVida(playerF.getCantidadVida()-20);  
                playerF.getImagenPrincipal().setImage(new Image("imagenes/sprites/Bill/herido.png"));
            }
            
       }
       
    }
    
    //revisando colision contra enemigo2
    public void checkCollision2(){
        if(this.collision(playerF2)){
            //System.out.println("CHOQUE ESFERA CONTRA ENEMIGO2 DETECTADO");
            timer.stop();
            if(playerF2.getCantidadVida()>0 && playerF2.getEstado().equals(Estado.AGACHADO)){
                PaneOrganizer2.playerF2.setCantidadVida(PaneOrganizer2.playerF2.getCantidadVida()-0);  
                //playerF2.getImagenPrincipal().setImage(new Image("imagenes/sprites/Freezer/Freezer21.png"));
            }
            else if(playerF2.getCantidadVida()>0&& !playerF2.getEstado().equals(Estado.AGACHADO)){
                PaneOrganizer2.playerF2.setCantidadVida(PaneOrganizer2.playerF2.getCantidadVida()-5);  
                playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/Img5.png"));
            }            
        }
        else if(this.collision(player)){
            //System.out.println("CHOQUE ESFERA CONTRA ENEMIGO2 DETECTADO");
            timer.stop();
            if(player.getCantidadVida()>0 && player.getEstado().equals(Estado.AGACHADO)){
                player.setCantidadVida(player.getCantidadVida()-5);  
                //player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/herido.png"));
            }
            else if(player.getCantidadVida()>0 && !player.getEstado().equals(Estado.SALTO)){
                player.setCantidadVida(player.getCantidadVida()-2);  
               // player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/herido.png"));
            } 
            
            else if(player.getCantidadVida()>0 && !player.getEstado().equals(Estado.AGACHADO)
                    && !player.getEstado().equals(Estado.SALTO)){
                player.setCantidadVida(player.getCantidadVida()-10);  
               // player.getImagenPrincipal().setImage(new Image("imagenes/sprites/Goku/herido.png"));
            } 
        }

    }
    
    //revisando colision de genkidama contra enemigo2
    public void checkCollisionGenki2(){
       if(this.collisionGenki2(playerF2)){
            //System.out.println("CHOQUE GENKIDAMA CONTRA ENEMIGO2 DETECTADO");
            timer.stop();
            if(playerF2.getCantidadVida()>0 && playerF2.getEstado().equals(Estado.AGACHADO)){
                playerF2.setCantidadVida(playerF2.getCantidadVida()-0);  
                //playerF2.getImagenPrincipal().setImage(new Image("imagenes/sprites/Freezer/Freezer21.png"));
            }
            else if(playerF2.getCantidadVida()>0 && !playerF2.getEstado().equals(Estado.AGACHADO)){
                PaneOrganizer2.playerF2.setCantidadVida(PaneOrganizer2.playerF2.getCantidadVida()-10);  
                playerF2.getImagenPrincipal().setImage(new Image("imagenes/broly/Img5.png"));
            }
            
       } 
       
    }
    //colision con jugador 
    public boolean collision(Player player){
        PathTransition transicion = this.getTransition();
        return player.rectanguloPlayer().intersects(transicion.getNode().translateXProperty().doubleValue(),
                                                transicion.getNode().translateYProperty().doubleValue(),
                                                this.ataqueGoku.getFitWidth(),this.ataqueGoku.getFitHeight());
    }
    
    //colision con freezer
    public boolean collision(Freezer playerF){
        PathTransition transicion = this.getTransition();
        return playerF.rectanguloPlayerF().intersects(transicion.getNode().translateXProperty().doubleValue(),
                                                transicion.getNode().translateYProperty().doubleValue(),
                                                 this.ataqueGoku.getFitWidth(),this.ataqueGoku.getFitHeight());
    }
    
    //colision con enemigo2
    public boolean collision(Enemigo2 playerF2){
        PathTransition transicion = this.getTransition();
        return playerF2.rectanguloEnemigo2().intersects(transicion.getNode().translateXProperty().doubleValue(),
                                                transicion.getNode().translateYProperty().doubleValue(),
                                                 this.ataqueGoku.getFitWidth(),this.ataqueGoku.getFitHeight());
    }
    
     //colision genkidama contra bills
    public boolean collisionGenki(Freezer playerF){
        PathTransition transicion = this.getTransition();
        return playerF.rectanguloPlayerF().intersects(transicion.getNode().translateXProperty().doubleValue(),
                                                transicion.getNode().translateYProperty().doubleValue(),
                                                 this.genkidama.getFitWidth(),this.genkidama.getFitHeight());
    }
    
     //colision genkidama contra enemigo2
    public boolean collisionGenki2(Enemigo2 playerF2){
        PathTransition transicion = this.getTransition();
        return playerF2.rectanguloEnemigo2().intersects(transicion.getNode().translateXProperty().doubleValue(),
                                                transicion.getNode().translateYProperty().doubleValue(),
                                                 this.genkidama.getFitWidth(),this.genkidama.getFitHeight());
    }
    
    public ImageView getAtaqueBill() {
        return ataqueBill;
    }

    public void setAtaqueBill(ImageView ataqueBill) {
        this.ataqueBill = ataqueBill;
    }

    public ImageView getAtaqueFinalBill() {
        return ataqueFinalBill;
    }

    public void setAtaqueFinalBill(ImageView ataqueFinalBill) {
        this.ataqueFinalBill = ataqueFinalBill;
    }

    public ImageView getAtaqueBroly() {
        return ataqueBroly;
    }

    public void setAtaqueBroly(ImageView ataqueBroly) {
        this.ataqueBroly = ataqueBroly;
    }

    public ImageView getAtaqueFinalBroly() {
        return ataqueFinalBroly;
    }

    public void setAtaqueFinalBroly(ImageView ataqueFinalBroly) {
        this.ataqueFinalBroly = ataqueFinalBroly;
    }

    public ImageView getAtaqueGoku() {
        return ataqueGoku;
    }

    public void setAtaqueGoku(ImageView ataqueGoku) {
        this.ataqueGoku = ataqueGoku;
    }

    public ImageView getGenkidama() {
        return genkidama;
    }

    public void setGenkidama(ImageView genkidama) {
        this.genkidama = genkidama;
    }
    
  
}

